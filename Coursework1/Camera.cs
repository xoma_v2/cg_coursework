﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework1
{
    public class Camera
    {
        public Point3D Position { get; set; }
        public float[,] Rotation { get; set; }

        public Camera(Point3D position, float[,] rotation = null)
        {
            Position = position;

            if (rotation != null)
                Rotation = rotation;
            else
                Rotation = new float[4, 4] { { 1, 0, 0, 0 },
                                             { 0, 1, 0, 0 },
                                             { 0, 0, 1, 0 },
                                             { 0, 0, 0, 1 } };
        }
    }
}
