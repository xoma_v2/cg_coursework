﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Coursework1
{
    public enum LightType { Ambient = 0, Point, Directional };

    public class Light
    {
        public LightType LType { get; set; }
        private float _intensity;
        public float Intensity
        {
            get { return _intensity; }
            set
            {
                _intensity = value;
                Intensitys[0] = _intensity * _color.R / 255;
                Intensitys[1] = _intensity * _color.G / 255;
                Intensitys[2] = _intensity * _color.B / 255;
            }
        }
        private Color _color;
        public Color color
        {
            get { return _color; }
            set
            {
                _color = value;
                Intensitys[0] = _intensity * _color.R / 255;
                Intensitys[1] = _intensity * _color.G / 255;
                Intensitys[2] = _intensity * _color.B / 255;
            }
        }
        public Point3D Position { get; set; }
        public float[] Intensitys { get; private set; } = new float[3] { 0, 0, 0 };

        public Light(LightType ltype, float intensity, Color color, Point3D position = null)
        {
            if (position == null && ltype != LightType.Ambient)
                throw new System.Exception("You must specify the position for all light sources except the ambient.");

            this.LType = ltype;
            this._intensity = intensity;
            this.color = color; // Здесь происходит заполнение массива поканальной интенсивности
            this.Position = position;
        }
    }
}
