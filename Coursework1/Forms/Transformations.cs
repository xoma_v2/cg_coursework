﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Coursework1.Forms
{
    public partial class Transformations : Form
    {
        public object Value { get; private set; }

        public Transformations(object obj)
        {
            InitializeComponent();
            this.Value = obj;

            if (obj.GetType().Name == "Sphere")
                tabControl1.TabPages.Remove(tabPageScaling);
            else if (obj.GetType().Name == "Cone")
            {
                tabControl1.TabPages.Remove(tabPageScaling);
                tabControl1.TabPages.Remove(tabPageRotation);
            }
            else if (obj.GetType().Name == "Cylinder")
            {
                tabControl1.TabPages.Remove(tabPageScaling);
                tabControl1.TabPages.Remove(tabPageRotation);
            }
            else if (Value.GetType().Name == "Light")
            {
                if (((Light)Value).LType != LightType.Point)
                {
                    MessageBox.Show("Аффинные преобразования могут применятся только к точечным источникам света.");
                    tabControl1.Enabled = false;
                    panel1.Enabled = false;
                }
                else
                {
                    radioButton2.Checked = true;
                    radioButton1.Enabled = false;
                }
            }
            else if (Value.GetType().Name == "Camera")
            {
                tabControl1.TabPages.Remove(tabPageScaling);
                tabControl1.TabPages.Remove(tabPageUniformScaling);
                panel1.Visible = false;
                label3.Visible = true;
                buttonCancel.Location = new Point(buttonCancel.Location.X, buttonCancel.Location.Y - 25);
            }
            //Parallelepiped
            //TetrahedralPyramid
            //TriangularPrism
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            buttonAdd.Enabled = false;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            buttonAdd.Enabled = true;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            buttonAdd.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab == tabControl1.TabPages["tabPageMove"])
                panel1.Enabled = false;
            else
                panel1.Enabled = true;
        }

        private void buttonRotate_Click(object sender, EventArgs e)
        {
            float ax = (float)numericUpDown1.Value;
            float ay = (float)numericUpDown2.Value;
            float az = (float)numericUpDown3.Value;

            if (Value.GetType().Name == "Sphere")
            {
                Sphere obj = (Sphere)Value;
                Point3D Center = (radioButton1.Checked == true) ? obj.GetAveragePoint() : Point3D.Parse(buttonAdd.Text);
                if (Center == null) { MessageBox.Show("Укажите точку, относительно которой будет выполняться преобразование."); return; }
                Value = new Sphere(obj.Center.Rotate(ax, ay, az, Center),
                                   obj.Radius,
                                   obj.Color, obj.Specular, obj.Reflective);
            }
            else if (Value.GetType().Name == "Parallelepiped")
            {
                Parallelepiped obj = (Parallelepiped)Value;
                Point3D Center = (radioButton1.Checked == true) ? obj.GetAveragePoint() : Point3D.Parse(buttonAdd.Text);
                if (Center == null) { MessageBox.Show("Укажите точку, относительно которой будет выполняться преобразование."); return; }
                Value = new Parallelepiped(obj.A.Rotate(ax, ay, az, Center),
                                           obj.D.Rotate(ax, ay, az, Center),
                                           obj.B.Rotate(ax, ay, az, Center),
                                           obj.A1.Rotate(ax, ay, az, Center),
                                           obj.Color, obj.Specular, obj.Reflective);
            }
            else if (Value.GetType().Name == "TetrahedralPyramid")
            {
                TetrahedralPyramid obj = (TetrahedralPyramid)Value;
                Point3D Center = (radioButton1.Checked == true) ? obj.GetAveragePoint() : Point3D.Parse(buttonAdd.Text);
                if (Center == null) { MessageBox.Show("Укажите точку, относительно которой будет выполняться преобразование."); return; }
                Value = new TetrahedralPyramid(obj.a.Rotate(ax, ay, az, Center),
                                               obj.b.Rotate(ax, ay, az, Center),
                                               obj.c.Rotate(ax, ay, az, Center),
                                               obj.d.Rotate(ax, ay, az, Center),
                                               obj.o.Rotate(ax, ay, az, Center),
                                               obj.Color, obj.Specular, obj.Reflective);
            }
            else if (Value.GetType().Name == "TriangularPrism")
            {
                TriangularPrism obj = (TriangularPrism)Value;
                Point3D Center = (radioButton1.Checked == true) ? obj.GetAveragePoint() : Point3D.Parse(buttonAdd.Text);
                if (Center == null) { MessageBox.Show("Укажите точку, относительно которой будет выполняться преобразование."); return; }
                Value = new TriangularPrism(obj.A.Rotate(ax, ay, az, Center),
                                            obj.C.Rotate(ax, ay, az, Center),
                                            obj.B.Rotate(ax, ay, az, Center),
                                            obj.A1.Rotate(ax, ay, az, Center),
                                            obj.Color, obj.Specular, obj.Reflective);
            }
            else if (Value.GetType().Name == "Light")
            {
                if (((Light)Value).LType == LightType.Point)
                {
                    Light obj = (Light)Value;
                    Point3D Center = Point3D.Parse(buttonAdd.Text);
                    if (Center == null) { MessageBox.Show("Укажите точку, относительно которой будет выполняться преобразование."); return; }
                    Value = new Light(obj.LType,
                                      obj.Intensity,
                                      obj.color,
                                      obj.Position.Rotate(ax, ay, az, Center));
                }
            }
            else if (Value.GetType().Name == "Camera")
            {
                ax = (float)(ax * Math.PI / 180);
                ay = (float)(ay * Math.PI / 180);
                az = (float)(az * Math.PI / 180);

                float[,] matrBX = new float[,] { { 1, 0,                    0,                   0 },
                                                 { 0, (float)Math.Cos(ax),  (float)Math.Sin(ax), 0 },
                                                 { 0, -(float)Math.Sin(ax), (float)Math.Cos(ax), 0 },
                                                 { 0, 0,                    0,                   1 } };
                float[,] matrBY = new float[,] { { (float)Math.Cos(ay),  0, (float)Math.Sin(ay), 0 },
                                                 { 0,                    1, 0,                   0 },
                                                 { -(float)Math.Sin(ay), 0, (float)Math.Cos(ay), 0 },
                                                 { 0,                    0, 0,                   1 } };
                float[,] matrBZ = new float[,] { { (float)Math.Cos(az),  (float)Math.Sin(az), 0, 0 },
                                                 { -(float)Math.Sin(az), (float)Math.Cos(az), 0, 0 },
                                                 { 0,                    0,                   1, 0 },
                                                 { 0,                    0,                   0, 1 } };
                
                matrBY = MyMath.MultiplyMM(matrBX, matrBY);
                matrBZ = MyMath.MultiplyMM(matrBY, matrBZ);

                Camera obj = (Camera)Value;
                Value = new Camera(obj.Position, MyMath.MultiplyMM(obj.Rotation, matrBZ));
            }
        }

        private void buttonMove_Click(object sender, EventArgs e)
        {
            float dx = (float)numericUpDown4.Value;
            float dy = (float)numericUpDown5.Value;
            float dz = (float)numericUpDown6.Value;

            if (Value.GetType().Name == "Sphere")
            {
                Sphere obj = (Sphere)Value;
                Value = new Sphere(obj.Center.Move(dx, dy, dz),
                                   obj.Radius,
                                   obj.Color, obj.Specular, obj.Reflective);
            }
            else if (Value.GetType().Name == "Parallelepiped")
            {
                Parallelepiped obj = (Parallelepiped)Value;
                Value = new Parallelepiped(obj.A.Move(dx, dy, dz),
                                           obj.D.Move(dx, dy, dz),
                                           obj.B.Move(dx, dy, dz),
                                           obj.A1.Move(dx, dy, dz),
                                           obj.Color, obj.Specular, obj.Reflective);
            }
            else if (Value.GetType().Name == "TetrahedralPyramid")
            {
                TetrahedralPyramid obj = (TetrahedralPyramid)Value;
                Value = new TetrahedralPyramid(obj.a.Move(dx, dy, dz),
                                               obj.b.Move(dx, dy, dz),
                                               obj.c.Move(dx, dy, dz),
                                               obj.d.Move(dx, dy, dz),
                                               obj.o.Move(dx, dy, dz),
                                               obj.Color, obj.Specular, obj.Reflective);
            }
            else if (Value.GetType().Name == "TriangularPrism")
            {
                TriangularPrism obj = (TriangularPrism)Value;
                Value = new TriangularPrism(obj.A.Move(dx, dy, dz),
                                            obj.C.Move(dx, dy, dz),
                                            obj.B.Move(dx, dy, dz),
                                            obj.A1.Move(dx, dy, dz),
                                            obj.Color, obj.Specular, obj.Reflective);
            }
            else if (Value.GetType().Name == "Cone")
            {
                Cone obj = (Cone)Value;
                Value = new Cone(obj.Center.Move(dx, dy, dz),
                                 obj.Radius,
                                 obj.Height,
                                 obj.Color, obj.Specular, obj.Reflective);
            }
            else if (Value.GetType().Name == "Cylinder")
            {
                Cylinder obj = (Cylinder)Value;
                Value = new Cylinder(obj.Center.Move(dx, dy, dz),
                                     obj.Radius,
                                     obj.Height,
                                     obj.Color, obj.Specular, obj.Reflective);
            }
            else if (Value.GetType().Name == "Light")
            {
                if (((Light)Value).LType == LightType.Point)
                {
                    Light obj = (Light)Value;
                    Value = new Light(obj.LType,
                                      obj.Intensity,
                                      obj.color,
                                      obj.Position.Move(dx, dy, dz));
                }
            }
            else if (Value.GetType().Name == "Camera")
            {
                Camera obj = (Camera)Value;
                Value = new Camera(obj.Position.Move(dx, dy, dz),
                                   obj.Rotation);
            }
        }

        private void buttonScaling_Click(object sender, EventArgs e)
        {
            float kx = (float)numericUpDown7.Value;
            float ky = (float)numericUpDown8.Value;
            float kz = (float)numericUpDown9.Value;

            if (Value.GetType().Name == "Parallelepiped")
            {
                Parallelepiped obj = (Parallelepiped)Value;
                Point3D Center = (radioButton1.Checked == true) ? obj.GetAveragePoint() : Point3D.Parse(buttonAdd.Text);
                if (Center == null) { MessageBox.Show("Укажите точку, относительно которой будет выполняться преобразование."); return; }
                Value = new Parallelepiped(obj.A.Scale(kx, ky, kz, Center),
                                           obj.D.Scale(kx, ky, kz, Center),
                                           obj.B.Scale(kx, ky, kz, Center),
                                           obj.A1.Scale(kx, ky, kz, Center),
                                           obj.Color, obj.Specular, obj.Reflective);
            }
            else if (Value.GetType().Name == "TetrahedralPyramid")
            {
                TetrahedralPyramid obj = (TetrahedralPyramid)Value;
                Point3D Center = (radioButton1.Checked == true) ? obj.GetAveragePoint() : Point3D.Parse(buttonAdd.Text);
                if (Center == null) { MessageBox.Show("Укажите точку, относительно которой будет выполняться преобразование."); return; }
                Value = new TetrahedralPyramid(obj.a.Scale(kx, ky, kz, Center),
                                               obj.b.Scale(kx, ky, kz, Center),
                                               obj.c.Scale(kx, ky, kz, Center),
                                               obj.d.Scale(kx, ky, kz, Center),
                                               obj.o.Scale(kx, ky, kz, Center),
                                               obj.Color, obj.Specular, obj.Reflective);
            }
            else if (Value.GetType().Name == "TriangularPrism")
            {
                TriangularPrism obj = (TriangularPrism)Value;
                Point3D Center = (radioButton1.Checked == true) ? obj.GetAveragePoint() : Point3D.Parse(buttonAdd.Text);
                if (Center == null) { MessageBox.Show("Укажите точку, относительно которой будет выполняться преобразование."); return; }
                Value = new TriangularPrism(obj.A.Scale(kx, ky, kz, Center),
                                            obj.C.Scale(kx, ky, kz, Center),
                                            obj.B.Scale(kx, ky, kz, Center),
                                            obj.A1.Scale(kx, ky, kz, Center),
                                            obj.Color, obj.Specular, obj.Reflective);
            }
            else if (Value.GetType().Name == "Light")
            {
                if (((Light)Value).LType == LightType.Point)
                {
                    Light obj = (Light)Value;
                    Point3D Center = Point3D.Parse(buttonAdd.Text);
                    if (Center == null) { MessageBox.Show("Укажите точку, относительно которой будет выполняться преобразование."); return; }
                    Value = new Light(obj.LType,
                                      obj.Intensity,
                                      obj.color,
                                      obj.Position.Scale(kx, ky, kz, Center));
                }
            }
        }

        private void buttonUniformScaling_Click(object sender, EventArgs e)
        {
            float k = (float)numericUpDown10.Value;

            if (Value.GetType().Name == "Sphere")
            {
                Sphere obj = (Sphere)Value;
                Point3D Center = (radioButton1.Checked == true) ? obj.GetAveragePoint() : Point3D.Parse(buttonAdd.Text);
                if (Center == null) { MessageBox.Show("Укажите точку, относительно которой будет выполняться преобразование."); return; }
                Value = new Sphere(obj.Center.Scale(k, Center),
                                   obj.Radius * k,
                                   obj.Color, obj.Specular, obj.Reflective);
            }
            else if (Value.GetType().Name == "Parallelepiped")
            {
                Parallelepiped obj = (Parallelepiped)Value;
                Point3D Center = (radioButton1.Checked == true) ? obj.GetAveragePoint() : Point3D.Parse(buttonAdd.Text);
                if (Center == null) { MessageBox.Show("Укажите точку, относительно которой будет выполняться преобразование."); return; }
                Value = new Parallelepiped(obj.A.Scale(k, Center),
                                           obj.D.Scale(k, Center),
                                           obj.B.Scale(k, Center),
                                           obj.A1.Scale(k, Center),
                                           obj.Color, obj.Specular, obj.Reflective);
            }
            else if (Value.GetType().Name == "TetrahedralPyramid")
            {
                TetrahedralPyramid obj = (TetrahedralPyramid)Value;
                Point3D Center = (radioButton1.Checked == true) ? obj.GetAveragePoint() : Point3D.Parse(buttonAdd.Text);
                if (Center == null) { MessageBox.Show("Укажите точку, относительно которой будет выполняться преобразование."); return; }
                Value = new TetrahedralPyramid(obj.a.Scale(k, Center),
                                               obj.b.Scale(k, Center),
                                               obj.c.Scale(k, Center),
                                               obj.d.Scale(k, Center),
                                               obj.o.Scale(k, Center),
                                               obj.Color, obj.Specular, obj.Reflective);
            }
            else if (Value.GetType().Name == "TriangularPrism")
            {
                TriangularPrism obj = (TriangularPrism)Value;
                Point3D Center = (radioButton1.Checked == true) ? obj.GetAveragePoint() : Point3D.Parse(buttonAdd.Text);
                if (Center == null) { MessageBox.Show("Укажите точку, относительно которой будет выполняться преобразование."); return; }
                Value = new TriangularPrism(obj.A.Scale(k, Center),
                                            obj.C.Scale(k, Center),
                                            obj.B.Scale(k, Center),
                                            obj.A1.Scale(k, Center),
                                            obj.Color, obj.Specular, obj.Reflective);
            }
            else if (Value.GetType().Name == "Cone")
            {
                Cone obj = (Cone)Value;
                Point3D Center = (radioButton1.Checked == true) ? obj.GetAveragePoint() : Point3D.Parse(buttonAdd.Text);
                if (Center == null) { MessageBox.Show("Укажите точку, относительно которой будет выполняться преобразование."); return; }
                Value = new Cone(obj.Center.Scale(k, Center),
                                 obj.Radius * k,
                                 obj.Height * k,
                                 obj.Color, obj.Specular, obj.Reflective);
            }
            else if (Value.GetType().Name == "Cylinder")
            {
                Cylinder obj = (Cylinder)Value;
                Point3D Center = (radioButton1.Checked == true) ? obj.GetAveragePoint() : Point3D.Parse(buttonAdd.Text);
                if (Center == null) { MessageBox.Show("Укажите точку, относительно которой будет выполняться преобразование."); return; }
                Value = new Cylinder(obj.Center.Scale(k, Center),
                                     obj.Radius * k,
                                     obj.Height * k,
                                     obj.Color, obj.Specular, obj.Reflective);
            }
            else if (Value.GetType().Name == "Light")
            {
                if (((Light)Value).LType == LightType.Point)
                {
                    Light obj = (Light)Value;
                    Point3D Center = Point3D.Parse(buttonAdd.Text);
                    if (Center == null) { MessageBox.Show("Укажите точку, относительно которой будет выполняться преобразование."); return; }
                    Value = new Light(obj.LType,
                                      obj.Intensity,
                                      obj.color,
                                      obj.Position.Scale(k, Center));
                }
            }
            else if (Value.GetType().Name == "Camera")
            {
                ;
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
