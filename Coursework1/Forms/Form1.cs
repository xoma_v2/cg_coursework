﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing.Imaging;

namespace Coursework1
{
    public partial class Form1 : Form
    {
        Bitmap bitmap;
        public RayTracer rt;

        //List<float> intensitys = new List<float>();
        int activeCameraIndex = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private string Translate(string name)
        {
            switch (name)
            {
                case "Cone":
                    return "Конус";
                case "Cylinder":
                    return "Цилиндр";
                case "Parallelepiped":
                    return "Параллелепипед";
                case "Sphere":
                    return "Сфера";
                case "TetrahedralPyramid":
                    return "Четырёхугольная пирамида";
                case "TriangularPrism":
                    return "Треугольная призма";
                case "Ambient":
                    return "Окружающий";
                case "Point":
                    return "Точечный";
                case "Directional":
                    return "Направленный";
                default:
                    return name;
            }
        }

        private void UpdateDataGridPrimitives()
        {
            int index = dataGridViewPrimitives.CurrentCellAddress.Y;

            dataGridViewPrimitives.Rows.Clear();
            int i = 0;
            foreach (var node in rt.primitives)
            {
                dataGridViewPrimitives.Rows.Add(i, Translate(node.GetType().Name));
                dataGridViewPrimitives.Rows[i].Cells[2].Style.BackColor = node.Color.Format24bppRgb;
                i++;
            }

            if (index != -1)
            {
                if (dataGridViewPrimitives.RowCount == 0)
                    ;
                else if (dataGridViewPrimitives.RowCount <= index)
                    dataGridViewPrimitives.CurrentCell = dataGridViewPrimitives[0, index - 1];
                else
                    dataGridViewPrimitives.CurrentCell = dataGridViewPrimitives[0, index];
            }
        }

        private void UpdateDataGridLights()
        {
            int index = dataGridViewLights.CurrentCellAddress.Y;

            dataGridViewLights.Rows.Clear();
            int i = 0;
            foreach (var node in rt.lights)
            {
                dataGridViewLights.Rows.Add(i, Translate(node.LType.ToString()), node.Intensity/*intensitys[i]*/);
                dataGridViewLights.Rows[i].Cells[3].Style.BackColor = node.color;
                i++;
            }

            if (index != -1)
            {
                if (dataGridViewLights.RowCount == 0)
                    ;
                else if (dataGridViewLights.RowCount <= index)
                    dataGridViewLights.CurrentCell = dataGridViewLights[0, index - 1];
                else
                    dataGridViewLights.CurrentCell = dataGridViewLights[0, index];
            }
        }

        private void UpdateDataGridCameras()
        {
            int index = dataGridViewCameras.CurrentCellAddress.Y;

            dataGridViewCameras.Rows.Clear();
            int i = 0;
            foreach (var node in rt.cameras)
            {
                dataGridViewCameras.Rows.Add(i, node.Position.X, node.Position.Y, node.Position.Z, activeCameraIndex == i ? '*' : ' ');
                i++;
            }

            if (index != -1)
            {
                if (dataGridViewCameras.RowCount == 0)
                    ;
                else if (dataGridViewCameras.RowCount <= index)
                    dataGridViewCameras.CurrentCell = dataGridViewCameras[0, index - 1];
                else
                    dataGridViewCameras.CurrentCell = dataGridViewCameras[0, index];
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            rt = new RayTracer(bitmap, Color.Black, 3);

            dataGridViewPrimitives.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
            dataGridViewPrimitives.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
            dataGridViewPrimitives.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;
            UpdateDataGridPrimitives();

            //foreach (var light in rt.lights)
            //    intensitys.Add(light.Intensity * 100);
            dataGridViewLights.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
            dataGridViewLights.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
            dataGridViewLights.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;
            dataGridViewLights.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;
            UpdateDataGridLights();

            dataGridViewCameras.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
            dataGridViewCameras.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
            dataGridViewCameras.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;
            dataGridViewCameras.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;
            dataGridViewCameras.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;
            UpdateDataGridCameras();

            buttonTrace_Click(sender, e);
        }

        private void pictureBox1_Resize(object sender, EventArgs e)
        {
            bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            rt.Canvas = bitmap;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            MouseEventArgs me = (MouseEventArgs)e;
            Point3D normal = new Point3D(0, 0, 0);
            int index = rt.FindClosest(me.Location.X, me.Location.Y, ref normal, activeCameraIndex);
            if (index != -1)
            {
                labelNormal.Text = normal.X.ToString() + " " + normal.Y.ToString() + " " + normal.Z.ToString();
                dataGridViewPrimitives.CurrentCell = dataGridViewPrimitives[0, index];
            }
        }

        private void buttonTrace_Click(object sender, EventArgs e)
        {
            panel1.Enabled = false;
            menuStrip1.Enabled = false;

            rt.Start(activeCameraIndex);
            pictureBox1.Image = rt.Canvas;

            panel1.Enabled = true;
            menuStrip1.Enabled = true;
        }

        private void обАвтореToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Программу выполнил студент группы ИУ7-52Б\nИксарица Н.И.");
        }

        private void оСистемеКоординатToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Ось OX направлена вправо из центра окна просмотра.\n" +
                            "Ось OY направлена вверх из центра окна просмотра.\n" +
                            "Ось OZ направлена на пользователя из центра окна просмотра.");
        }

        private void выходИзПрограммыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonAddPrimitive_Click(object sender, EventArgs e)
        {
            Forms.PrimitiveAdd form = new Forms.PrimitiveAdd(rt.primitives);
            form.ShowDialog();

            if (rt.primitives.Count != dataGridViewPrimitives.Rows.Count)
                UpdateDataGridPrimitives();
        }

        private void buttonDeletePrimitive_Click(object sender, EventArgs e)
        {
            Point p = dataGridViewPrimitives.CurrentCellAddress;
            if (p != new Point(-1, -1))
            {
                int index = (int)dataGridViewPrimitives[0, dataGridViewPrimitives.CurrentCellAddress.Y].Value;
                rt.primitives.RemoveAt(index);
                //dataGridViewPrimitives.Rows.RemoveAt(dataGridViewPrimitives.SelectedRows[0].Index);
                UpdateDataGridPrimitives();
            }
        }

        private void buttonConfigPrimitive_Click(object sender, EventArgs e)
        {
            Point p = dataGridViewPrimitives.CurrentCellAddress;
            if (p != new Point(-1, -1))
            {
                int index = (int)dataGridViewPrimitives[0, dataGridViewPrimitives.CurrentCellAddress.Y].Value;
                Forms.PrimitiveConfig form = new Forms.PrimitiveConfig(rt.primitives[index]);
                form.ShowDialog();
                if (form.Value != rt.primitives[index])
                {
                    rt.primitives[index] = form.Value;
                    UpdateDataGridPrimitives();
                }
            }
        }

        private void buttonTransformPrimitive_Click(object sender, EventArgs e)
        {
            Point p = dataGridViewPrimitives.CurrentCellAddress;
            if (p != new Point(-1, -1))
            {
                int index = (int)dataGridViewPrimitives[0, dataGridViewPrimitives.CurrentCellAddress.Y].Value;
                Forms.Transformations form = new Forms.Transformations(rt.primitives[index]);
                form.ShowDialog();
                if (form.Value != rt.primitives[index])
                {
                    rt.primitives[index] = (Primitive)form.Value;
                    UpdateDataGridPrimitives();
                }
            }
        }

        private void buttonAddLight_Click(object sender, EventArgs e)
        {
            Forms.LightAdd form = new Forms.LightAdd(rt.lights/*, intensitys*/);
            form.ShowDialog();

            if (rt.lights.Count != dataGridViewLights.Rows.Count)
                UpdateDataGridLights();
        }

        private void buttonDeleteLight_Click(object sender, EventArgs e)
        {
            Point p = dataGridViewLights.CurrentCellAddress;
            if (p != new Point(-1, -1))
            {
                int index = (int)dataGridViewLights[0, dataGridViewLights.CurrentCellAddress.Y].Value;
                rt.lights.RemoveAt(index);
                //intensitys.RemoveAt(index);
                UpdateDataGridLights();

                //float sum = intensitys.Sum();
                //for (int i = 0; i < rt.lights.Count; i++)
                //    rt.lights[i].Intensity = intensitys[i] / sum;
            }
        }

        private void buttonConfigLight_Click(object sender, EventArgs e)
        {
            Point p = dataGridViewLights.CurrentCellAddress;
            if (p != new Point(-1, -1))
            {
                int index = (int)dataGridViewLights[0, dataGridViewLights.CurrentCellAddress.Y].Value;
                Forms.LightConfig form = new Forms.LightConfig(rt.lights[index]);
                form.ShowDialog();
                if (form.Value != rt.lights[index])
                {
                    rt.lights[index] = form.Value;
                    UpdateDataGridLights();
                }
            }
        }

        private void buttonTransformLight_Click(object sender, EventArgs e)
        {
            Point p = dataGridViewLights.CurrentCellAddress;
            if (p != new Point(-1, -1))
            {
                int index = (int)dataGridViewLights[0, dataGridViewLights.CurrentCellAddress.Y].Value;
                if (rt.lights[index].LType != LightType.Point) { MessageBox.Show("Афинные преобразования могут применятся только к точечным источникам света."); return; }
                Forms.Transformations form = new Forms.Transformations(rt.lights[index]);
                form.ShowDialog();
                if (form.Value != rt.lights[index])
                {
                    rt.lights[index] = (Light)form.Value;
                    UpdateDataGridLights();
                }
            }
        }

        private void buttonAddCamera_Click(object sender, EventArgs e)
        {
            Forms.CameraAdd form = new Forms.CameraAdd(rt.cameras);
            form.ShowDialog();

            if (rt.cameras.Count != dataGridViewCameras.Rows.Count)
                UpdateDataGridCameras();
        }

        private void buttonDeleteCamera_Click(object sender, EventArgs e)
        {
            Point p = dataGridViewCameras.CurrentCellAddress;
            if (p != new Point(-1, -1))
            {
                if (rt.cameras.Count != 1)
                {
                    int index = (int)dataGridViewCameras[0, dataGridViewCameras.CurrentCellAddress.Y].Value;
                    rt.cameras.RemoveAt(index);

                    if (index < activeCameraIndex)
                        activeCameraIndex -= 1;
                    else if (index == activeCameraIndex && index != 0)
                        activeCameraIndex -= 1;

                    UpdateDataGridCameras();
                }
                else
                    MessageBox.Show("На сцене должно быть не менее одной камеры.");
            }
        }

        private void buttonConfigCamera_Click(object sender, EventArgs e)
        {
            Point p = dataGridViewCameras.CurrentCellAddress;
            if (p != new Point(-1, -1))
            {
                int index = (int)dataGridViewCameras[0, dataGridViewCameras.CurrentCellAddress.Y].Value;
                Forms.CameraConfig form = new Forms.CameraConfig(rt.cameras[index]);
                form.ShowDialog();
                if (form.Value != rt.cameras[index])
                {
                    rt.cameras[index] = form.Value;
                    UpdateDataGridCameras();
                }
            }
        }

        private void buttonSetActiveCamera_Click(object sender, EventArgs e)
        {
            Point p = dataGridViewCameras.CurrentCellAddress;
            if (p != new Point(-1, -1))
            {
                int index = (int)dataGridViewCameras[0, dataGridViewCameras.CurrentCellAddress.Y].Value;
                activeCameraIndex = index;
                UpdateDataGridCameras();
            }
        }

        private void buttonTransformCamera_Click(object sender, EventArgs e)
        {
            Point p = dataGridViewCameras.CurrentCellAddress;
            if (p != new Point(-1, -1))
            {
                int index = (int)dataGridViewCameras[0, dataGridViewCameras.CurrentCellAddress.Y].Value;
                Forms.Transformations form = new Forms.Transformations(rt.cameras[index]);
                form.ShowDialog();
                if (form.Value != rt.cameras[index])
                {
                    rt.cameras[index] = (Camera)form.Value;
                    UpdateDataGridCameras();
                }
            }
        }

        private void настройкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Forms.SceneSettings form = new Forms.SceneSettings(rt);
            form.ShowDialog();
        }
    }
}
