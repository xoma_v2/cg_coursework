﻿namespace Coursework1.Forms
{
    partial class CameraAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAdd = new System.Windows.Forms.Button();
            this.labelZ = new System.Windows.Forms.Label();
            this.labelY = new System.Windows.Forms.Label();
            this.labelX = new System.Windows.Forms.Label();
            this.numericUpDownZ = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownY = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownX = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown11 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown21 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown31 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown41 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown42 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown32 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown22 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown12 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown43 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown33 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown23 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown13 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown44 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown34 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown24 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown14 = new System.Windows.Forms.NumericUpDown();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown14)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(49, 211);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(316, 41);
            this.buttonAdd.TabIndex = 40;
            this.buttonAdd.Text = "Добавить";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // labelZ
            // 
            this.labelZ.AutoSize = true;
            this.labelZ.Location = new System.Drawing.Point(11, 113);
            this.labelZ.Name = "labelZ";
            this.labelZ.Size = new System.Drawing.Size(19, 20);
            this.labelZ.TabIndex = 39;
            this.labelZ.Text = "Z";
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(11, 81);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(20, 20);
            this.labelY.TabIndex = 38;
            this.labelY.Text = "Y";
            // 
            // labelX
            // 
            this.labelX.AutoSize = true;
            this.labelX.Location = new System.Drawing.Point(11, 49);
            this.labelX.Name = "labelX";
            this.labelX.Size = new System.Drawing.Size(20, 20);
            this.labelX.TabIndex = 37;
            this.labelX.Text = "X";
            // 
            // numericUpDownZ
            // 
            this.numericUpDownZ.DecimalPlaces = 2;
            this.numericUpDownZ.Location = new System.Drawing.Point(37, 111);
            this.numericUpDownZ.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownZ.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDownZ.Name = "numericUpDownZ";
            this.numericUpDownZ.Size = new System.Drawing.Size(120, 26);
            this.numericUpDownZ.TabIndex = 36;
            // 
            // numericUpDownY
            // 
            this.numericUpDownY.DecimalPlaces = 2;
            this.numericUpDownY.Location = new System.Drawing.Point(37, 79);
            this.numericUpDownY.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownY.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDownY.Name = "numericUpDownY";
            this.numericUpDownY.Size = new System.Drawing.Size(120, 26);
            this.numericUpDownY.TabIndex = 35;
            // 
            // numericUpDownX
            // 
            this.numericUpDownX.DecimalPlaces = 2;
            this.numericUpDownX.Location = new System.Drawing.Point(37, 47);
            this.numericUpDownX.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownX.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDownX.Name = "numericUpDownX";
            this.numericUpDownX.Size = new System.Drawing.Size(120, 26);
            this.numericUpDownX.TabIndex = 34;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 20);
            this.label1.TabIndex = 41;
            this.label1.Text = "Положение камеры:";
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(371, 211);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(316, 41);
            this.buttonCancel.TabIndex = 42;
            this.buttonCancel.Text = "Закрыть";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(209, 20);
            this.label2.TabIndex = 46;
            this.label2.Text = "Матрица преобразования:";
            // 
            // numericUpDown11
            // 
            this.numericUpDown11.DecimalPlaces = 2;
            this.numericUpDown11.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDown11.Location = new System.Drawing.Point(17, 47);
            this.numericUpDown11.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown11.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown11.Name = "numericUpDown11";
            this.numericUpDown11.Size = new System.Drawing.Size(120, 26);
            this.numericUpDown11.TabIndex = 43;
            this.numericUpDown11.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericUpDown21
            // 
            this.numericUpDown21.DecimalPlaces = 2;
            this.numericUpDown21.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDown21.Location = new System.Drawing.Point(17, 79);
            this.numericUpDown21.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown21.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown21.Name = "numericUpDown21";
            this.numericUpDown21.Size = new System.Drawing.Size(120, 26);
            this.numericUpDown21.TabIndex = 60;
            // 
            // numericUpDown31
            // 
            this.numericUpDown31.DecimalPlaces = 2;
            this.numericUpDown31.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDown31.Location = new System.Drawing.Point(17, 111);
            this.numericUpDown31.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown31.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown31.Name = "numericUpDown31";
            this.numericUpDown31.Size = new System.Drawing.Size(120, 26);
            this.numericUpDown31.TabIndex = 61;
            // 
            // numericUpDown41
            // 
            this.numericUpDown41.DecimalPlaces = 2;
            this.numericUpDown41.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDown41.Location = new System.Drawing.Point(17, 143);
            this.numericUpDown41.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown41.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown41.Name = "numericUpDown41";
            this.numericUpDown41.Size = new System.Drawing.Size(120, 26);
            this.numericUpDown41.TabIndex = 62;
            // 
            // numericUpDown42
            // 
            this.numericUpDown42.DecimalPlaces = 2;
            this.numericUpDown42.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDown42.Location = new System.Drawing.Point(143, 143);
            this.numericUpDown42.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown42.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown42.Name = "numericUpDown42";
            this.numericUpDown42.Size = new System.Drawing.Size(120, 26);
            this.numericUpDown42.TabIndex = 66;
            // 
            // numericUpDown32
            // 
            this.numericUpDown32.DecimalPlaces = 2;
            this.numericUpDown32.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDown32.Location = new System.Drawing.Point(143, 111);
            this.numericUpDown32.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown32.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown32.Name = "numericUpDown32";
            this.numericUpDown32.Size = new System.Drawing.Size(120, 26);
            this.numericUpDown32.TabIndex = 65;
            // 
            // numericUpDown22
            // 
            this.numericUpDown22.DecimalPlaces = 2;
            this.numericUpDown22.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDown22.Location = new System.Drawing.Point(143, 79);
            this.numericUpDown22.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown22.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown22.Name = "numericUpDown22";
            this.numericUpDown22.Size = new System.Drawing.Size(120, 26);
            this.numericUpDown22.TabIndex = 64;
            this.numericUpDown22.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericUpDown12
            // 
            this.numericUpDown12.DecimalPlaces = 2;
            this.numericUpDown12.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDown12.Location = new System.Drawing.Point(143, 47);
            this.numericUpDown12.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown12.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown12.Name = "numericUpDown12";
            this.numericUpDown12.Size = new System.Drawing.Size(120, 26);
            this.numericUpDown12.TabIndex = 63;
            // 
            // numericUpDown43
            // 
            this.numericUpDown43.DecimalPlaces = 2;
            this.numericUpDown43.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDown43.Location = new System.Drawing.Point(269, 143);
            this.numericUpDown43.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown43.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown43.Name = "numericUpDown43";
            this.numericUpDown43.Size = new System.Drawing.Size(120, 26);
            this.numericUpDown43.TabIndex = 70;
            // 
            // numericUpDown33
            // 
            this.numericUpDown33.DecimalPlaces = 2;
            this.numericUpDown33.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDown33.Location = new System.Drawing.Point(269, 111);
            this.numericUpDown33.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown33.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown33.Name = "numericUpDown33";
            this.numericUpDown33.Size = new System.Drawing.Size(120, 26);
            this.numericUpDown33.TabIndex = 69;
            this.numericUpDown33.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericUpDown23
            // 
            this.numericUpDown23.DecimalPlaces = 2;
            this.numericUpDown23.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDown23.Location = new System.Drawing.Point(269, 79);
            this.numericUpDown23.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown23.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown23.Name = "numericUpDown23";
            this.numericUpDown23.Size = new System.Drawing.Size(120, 26);
            this.numericUpDown23.TabIndex = 68;
            // 
            // numericUpDown13
            // 
            this.numericUpDown13.DecimalPlaces = 2;
            this.numericUpDown13.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDown13.Location = new System.Drawing.Point(269, 47);
            this.numericUpDown13.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown13.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown13.Name = "numericUpDown13";
            this.numericUpDown13.Size = new System.Drawing.Size(120, 26);
            this.numericUpDown13.TabIndex = 67;
            // 
            // numericUpDown44
            // 
            this.numericUpDown44.DecimalPlaces = 2;
            this.numericUpDown44.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDown44.Location = new System.Drawing.Point(395, 143);
            this.numericUpDown44.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown44.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown44.Name = "numericUpDown44";
            this.numericUpDown44.Size = new System.Drawing.Size(120, 26);
            this.numericUpDown44.TabIndex = 74;
            this.numericUpDown44.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericUpDown34
            // 
            this.numericUpDown34.DecimalPlaces = 2;
            this.numericUpDown34.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDown34.Location = new System.Drawing.Point(395, 111);
            this.numericUpDown34.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown34.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown34.Name = "numericUpDown34";
            this.numericUpDown34.Size = new System.Drawing.Size(120, 26);
            this.numericUpDown34.TabIndex = 73;
            // 
            // numericUpDown24
            // 
            this.numericUpDown24.DecimalPlaces = 2;
            this.numericUpDown24.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDown24.Location = new System.Drawing.Point(395, 79);
            this.numericUpDown24.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown24.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown24.Name = "numericUpDown24";
            this.numericUpDown24.Size = new System.Drawing.Size(120, 26);
            this.numericUpDown24.TabIndex = 72;
            // 
            // numericUpDown14
            // 
            this.numericUpDown14.DecimalPlaces = 2;
            this.numericUpDown14.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDown14.Location = new System.Drawing.Point(395, 47);
            this.numericUpDown14.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown14.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown14.Name = "numericUpDown14";
            this.numericUpDown14.Size = new System.Drawing.Size(120, 26);
            this.numericUpDown14.TabIndex = 71;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.numericUpDownX);
            this.panel1.Controls.Add(this.numericUpDownY);
            this.panel1.Controls.Add(this.numericUpDownZ);
            this.panel1.Controls.Add(this.labelX);
            this.panel1.Controls.Add(this.labelY);
            this.panel1.Controls.Add(this.labelZ);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(180, 186);
            this.panel1.TabIndex = 75;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.numericUpDown11);
            this.panel2.Controls.Add(this.numericUpDown44);
            this.panel2.Controls.Add(this.numericUpDown21);
            this.panel2.Controls.Add(this.numericUpDown34);
            this.panel2.Controls.Add(this.numericUpDown31);
            this.panel2.Controls.Add(this.numericUpDown24);
            this.panel2.Controls.Add(this.numericUpDown41);
            this.panel2.Controls.Add(this.numericUpDown14);
            this.panel2.Controls.Add(this.numericUpDown12);
            this.panel2.Controls.Add(this.numericUpDown43);
            this.panel2.Controls.Add(this.numericUpDown22);
            this.panel2.Controls.Add(this.numericUpDown33);
            this.panel2.Controls.Add(this.numericUpDown32);
            this.panel2.Controls.Add(this.numericUpDown23);
            this.panel2.Controls.Add(this.numericUpDown42);
            this.panel2.Controls.Add(this.numericUpDown13);
            this.panel2.Location = new System.Drawing.Point(208, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(533, 186);
            this.panel2.TabIndex = 76;
            // 
            // CameraAdd
            // 
            this.AcceptButton = this.buttonAdd;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(755, 266);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonAdd);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(771, 305);
            this.MinimumSize = new System.Drawing.Size(771, 305);
            this.Name = "CameraAdd";
            this.Text = "Добавление камер";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown14)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Label labelZ;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Label labelX;
        private System.Windows.Forms.NumericUpDown numericUpDownZ;
        private System.Windows.Forms.NumericUpDown numericUpDownY;
        private System.Windows.Forms.NumericUpDown numericUpDownX;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown11;
        private System.Windows.Forms.NumericUpDown numericUpDown21;
        private System.Windows.Forms.NumericUpDown numericUpDown31;
        private System.Windows.Forms.NumericUpDown numericUpDown41;
        private System.Windows.Forms.NumericUpDown numericUpDown42;
        private System.Windows.Forms.NumericUpDown numericUpDown32;
        private System.Windows.Forms.NumericUpDown numericUpDown22;
        private System.Windows.Forms.NumericUpDown numericUpDown12;
        private System.Windows.Forms.NumericUpDown numericUpDown43;
        private System.Windows.Forms.NumericUpDown numericUpDown33;
        private System.Windows.Forms.NumericUpDown numericUpDown23;
        private System.Windows.Forms.NumericUpDown numericUpDown13;
        private System.Windows.Forms.NumericUpDown numericUpDown44;
        private System.Windows.Forms.NumericUpDown numericUpDown34;
        private System.Windows.Forms.NumericUpDown numericUpDown24;
        private System.Windows.Forms.NumericUpDown numericUpDown14;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
    }
}