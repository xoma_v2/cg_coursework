﻿namespace Coursework1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonTrace = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPagePrimitives = new System.Windows.Forms.TabPage();
            this.buttonTransformPrimitive = new System.Windows.Forms.Button();
            this.buttonAddPrimitive = new System.Windows.Forms.Button();
            this.dataGridViewPrimitives = new System.Windows.Forms.DataGridView();
            this.PrimitiveIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrimitiveType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrimitiveColor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonDeletePrimitive = new System.Windows.Forms.Button();
            this.buttonConfigPrimitive = new System.Windows.Forms.Button();
            this.tabPageLights = new System.Windows.Forms.TabPage();
            this.buttonTransformLight = new System.Windows.Forms.Button();
            this.buttonAddLight = new System.Windows.Forms.Button();
            this.dataGridViewLights = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonDeleteLight = new System.Windows.Forms.Button();
            this.buttonConfigLight = new System.Windows.Forms.Button();
            this.tabPageCameras = new System.Windows.Forms.TabPage();
            this.buttonTransformCamera = new System.Windows.Forms.Button();
            this.buttonSetActiveCamera = new System.Windows.Forms.Button();
            this.buttonAddCamera = new System.Windows.Forms.Button();
            this.dataGridViewCameras = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonDeleteCamera = new System.Windows.Forms.Button();
            this.buttonConfigCamera = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.обАвтореToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.настройкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходИзПрограммыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelNormal = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPagePrimitives.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPrimitives)).BeginInit();
            this.tabPageLights.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLights)).BeginInit();
            this.tabPageCameras.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCameras)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonTrace);
            this.panel1.Controls.Add(this.tabControl1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(721, 25);
            this.panel1.Margin = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(360, 721);
            this.panel1.TabIndex = 2;
            // 
            // buttonTrace
            // 
            this.buttonTrace.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.buttonTrace.Location = new System.Drawing.Point(13, 653);
            this.buttonTrace.Name = "buttonTrace";
            this.buttonTrace.Size = new System.Drawing.Size(330, 56);
            this.buttonTrace.TabIndex = 5;
            this.buttonTrace.Text = "Построить сцену";
            this.buttonTrace.UseVisualStyleBackColor = false;
            this.buttonTrace.Click += new System.EventHandler(this.buttonTrace_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPagePrimitives);
            this.tabControl1.Controls.Add(this.tabPageLights);
            this.tabControl1.Controls.Add(this.tabPageCameras);
            this.tabControl1.Location = new System.Drawing.Point(0, 5);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(360, 644);
            this.tabControl1.TabIndex = 4;
            // 
            // tabPagePrimitives
            // 
            this.tabPagePrimitives.Controls.Add(this.buttonTransformPrimitive);
            this.tabPagePrimitives.Controls.Add(this.buttonAddPrimitive);
            this.tabPagePrimitives.Controls.Add(this.dataGridViewPrimitives);
            this.tabPagePrimitives.Controls.Add(this.buttonDeletePrimitive);
            this.tabPagePrimitives.Controls.Add(this.buttonConfigPrimitive);
            this.tabPagePrimitives.Location = new System.Drawing.Point(4, 29);
            this.tabPagePrimitives.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPagePrimitives.Name = "tabPagePrimitives";
            this.tabPagePrimitives.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPagePrimitives.Size = new System.Drawing.Size(352, 611);
            this.tabPagePrimitives.TabIndex = 0;
            this.tabPagePrimitives.Text = "Примитивы";
            this.tabPagePrimitives.UseVisualStyleBackColor = true;
            // 
            // buttonTransformPrimitive
            // 
            this.buttonTransformPrimitive.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonTransformPrimitive.Location = new System.Drawing.Point(9, 418);
            this.buttonTransformPrimitive.Name = "buttonTransformPrimitive";
            this.buttonTransformPrimitive.Size = new System.Drawing.Size(330, 46);
            this.buttonTransformPrimitive.TabIndex = 4;
            this.buttonTransformPrimitive.Text = "Применить аффинные преобразования к\r\nвыделенному примитиву\r\n";
            this.buttonTransformPrimitive.UseVisualStyleBackColor = true;
            this.buttonTransformPrimitive.Click += new System.EventHandler(this.buttonTransformPrimitive_Click);
            // 
            // buttonAddPrimitive
            // 
            this.buttonAddPrimitive.Location = new System.Drawing.Point(179, 472);
            this.buttonAddPrimitive.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonAddPrimitive.Name = "buttonAddPrimitive";
            this.buttonAddPrimitive.Size = new System.Drawing.Size(160, 77);
            this.buttonAddPrimitive.TabIndex = 3;
            this.buttonAddPrimitive.Text = "[+] Добавить примитив";
            this.buttonAddPrimitive.UseVisualStyleBackColor = true;
            this.buttonAddPrimitive.Click += new System.EventHandler(this.buttonAddPrimitive_Click);
            // 
            // dataGridViewPrimitives
            // 
            this.dataGridViewPrimitives.AllowUserToAddRows = false;
            this.dataGridViewPrimitives.AllowUserToDeleteRows = false;
            this.dataGridViewPrimitives.AllowUserToResizeColumns = false;
            this.dataGridViewPrimitives.AllowUserToResizeRows = false;
            this.dataGridViewPrimitives.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPrimitives.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PrimitiveIndex,
            this.PrimitiveType,
            this.PrimitiveColor});
            this.dataGridViewPrimitives.Location = new System.Drawing.Point(9, 9);
            this.dataGridViewPrimitives.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dataGridViewPrimitives.MultiSelect = false;
            this.dataGridViewPrimitives.Name = "dataGridViewPrimitives";
            this.dataGridViewPrimitives.ReadOnly = true;
            this.dataGridViewPrimitives.RowHeadersVisible = false;
            this.dataGridViewPrimitives.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewPrimitives.Size = new System.Drawing.Size(330, 345);
            this.dataGridViewPrimitives.TabIndex = 0;
            // 
            // PrimitiveIndex
            // 
            this.PrimitiveIndex.HeaderText = "№";
            this.PrimitiveIndex.Name = "PrimitiveIndex";
            this.PrimitiveIndex.ReadOnly = true;
            this.PrimitiveIndex.Width = 55;
            // 
            // PrimitiveType
            // 
            this.PrimitiveType.HeaderText = "Примитив";
            this.PrimitiveType.Name = "PrimitiveType";
            this.PrimitiveType.ReadOnly = true;
            this.PrimitiveType.Width = 195;
            // 
            // PrimitiveColor
            // 
            this.PrimitiveColor.HeaderText = "Цвет";
            this.PrimitiveColor.Name = "PrimitiveColor";
            this.PrimitiveColor.ReadOnly = true;
            this.PrimitiveColor.Width = 55;
            // 
            // buttonDeletePrimitive
            // 
            this.buttonDeletePrimitive.Location = new System.Drawing.Point(9, 472);
            this.buttonDeletePrimitive.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonDeletePrimitive.Name = "buttonDeletePrimitive";
            this.buttonDeletePrimitive.Size = new System.Drawing.Size(160, 77);
            this.buttonDeletePrimitive.TabIndex = 2;
            this.buttonDeletePrimitive.Text = "[-] Удалить выделенный примитив";
            this.buttonDeletePrimitive.UseVisualStyleBackColor = true;
            this.buttonDeletePrimitive.Click += new System.EventHandler(this.buttonDeletePrimitive_Click);
            // 
            // buttonConfigPrimitive
            // 
            this.buttonConfigPrimitive.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonConfigPrimitive.Location = new System.Drawing.Point(9, 364);
            this.buttonConfigPrimitive.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonConfigPrimitive.Name = "buttonConfigPrimitive";
            this.buttonConfigPrimitive.Size = new System.Drawing.Size(330, 46);
            this.buttonConfigPrimitive.TabIndex = 1;
            this.buttonConfigPrimitive.Text = "Открыть/изменить свойства выделенного примитива";
            this.buttonConfigPrimitive.UseVisualStyleBackColor = true;
            this.buttonConfigPrimitive.Click += new System.EventHandler(this.buttonConfigPrimitive_Click);
            // 
            // tabPageLights
            // 
            this.tabPageLights.Controls.Add(this.buttonTransformLight);
            this.tabPageLights.Controls.Add(this.buttonAddLight);
            this.tabPageLights.Controls.Add(this.dataGridViewLights);
            this.tabPageLights.Controls.Add(this.buttonDeleteLight);
            this.tabPageLights.Controls.Add(this.buttonConfigLight);
            this.tabPageLights.Location = new System.Drawing.Point(4, 29);
            this.tabPageLights.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPageLights.Name = "tabPageLights";
            this.tabPageLights.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPageLights.Size = new System.Drawing.Size(352, 611);
            this.tabPageLights.TabIndex = 1;
            this.tabPageLights.Text = "Источники света";
            this.tabPageLights.UseVisualStyleBackColor = true;
            // 
            // buttonTransformLight
            // 
            this.buttonTransformLight.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonTransformLight.Location = new System.Drawing.Point(9, 418);
            this.buttonTransformLight.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonTransformLight.Name = "buttonTransformLight";
            this.buttonTransformLight.Size = new System.Drawing.Size(330, 46);
            this.buttonTransformLight.TabIndex = 8;
            this.buttonTransformLight.Text = "Применить аффинные преобразования к\r\nвыделенному источнику света";
            this.buttonTransformLight.UseVisualStyleBackColor = true;
            this.buttonTransformLight.Click += new System.EventHandler(this.buttonTransformLight_Click);
            // 
            // buttonAddLight
            // 
            this.buttonAddLight.Location = new System.Drawing.Point(179, 472);
            this.buttonAddLight.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonAddLight.Name = "buttonAddLight";
            this.buttonAddLight.Size = new System.Drawing.Size(160, 77);
            this.buttonAddLight.TabIndex = 7;
            this.buttonAddLight.Text = "[+] Добавить источник света";
            this.buttonAddLight.UseVisualStyleBackColor = true;
            this.buttonAddLight.Click += new System.EventHandler(this.buttonAddLight_Click);
            // 
            // dataGridViewLights
            // 
            this.dataGridViewLights.AllowUserToAddRows = false;
            this.dataGridViewLights.AllowUserToDeleteRows = false;
            this.dataGridViewLights.AllowUserToResizeColumns = false;
            this.dataGridViewLights.AllowUserToResizeRows = false;
            this.dataGridViewLights.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLights.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.Column3});
            this.dataGridViewLights.Location = new System.Drawing.Point(9, 9);
            this.dataGridViewLights.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dataGridViewLights.MultiSelect = false;
            this.dataGridViewLights.Name = "dataGridViewLights";
            this.dataGridViewLights.ReadOnly = true;
            this.dataGridViewLights.RowHeadersVisible = false;
            this.dataGridViewLights.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewLights.Size = new System.Drawing.Size(330, 345);
            this.dataGridViewLights.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "№";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 55;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Источник света";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 124;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Интенсивность";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 63;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Цвет";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 63;
            // 
            // buttonDeleteLight
            // 
            this.buttonDeleteLight.Location = new System.Drawing.Point(9, 472);
            this.buttonDeleteLight.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonDeleteLight.Name = "buttonDeleteLight";
            this.buttonDeleteLight.Size = new System.Drawing.Size(160, 77);
            this.buttonDeleteLight.TabIndex = 6;
            this.buttonDeleteLight.Text = "[-] Удалить выделенный источник света";
            this.buttonDeleteLight.UseVisualStyleBackColor = true;
            this.buttonDeleteLight.Click += new System.EventHandler(this.buttonDeleteLight_Click);
            // 
            // buttonConfigLight
            // 
            this.buttonConfigLight.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonConfigLight.Location = new System.Drawing.Point(9, 364);
            this.buttonConfigLight.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonConfigLight.Name = "buttonConfigLight";
            this.buttonConfigLight.Size = new System.Drawing.Size(330, 46);
            this.buttonConfigLight.TabIndex = 5;
            this.buttonConfigLight.Text = "Открыть/изменить свойства выделенного источника света";
            this.buttonConfigLight.UseVisualStyleBackColor = true;
            this.buttonConfigLight.Click += new System.EventHandler(this.buttonConfigLight_Click);
            // 
            // tabPageCameras
            // 
            this.tabPageCameras.Controls.Add(this.buttonTransformCamera);
            this.tabPageCameras.Controls.Add(this.buttonSetActiveCamera);
            this.tabPageCameras.Controls.Add(this.buttonAddCamera);
            this.tabPageCameras.Controls.Add(this.dataGridViewCameras);
            this.tabPageCameras.Controls.Add(this.buttonDeleteCamera);
            this.tabPageCameras.Controls.Add(this.buttonConfigCamera);
            this.tabPageCameras.Location = new System.Drawing.Point(4, 29);
            this.tabPageCameras.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPageCameras.Name = "tabPageCameras";
            this.tabPageCameras.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPageCameras.Size = new System.Drawing.Size(352, 611);
            this.tabPageCameras.TabIndex = 2;
            this.tabPageCameras.Text = "Камеры";
            this.tabPageCameras.UseVisualStyleBackColor = true;
            // 
            // buttonTransformCamera
            // 
            this.buttonTransformCamera.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonTransformCamera.Location = new System.Drawing.Point(9, 418);
            this.buttonTransformCamera.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonTransformCamera.Name = "buttonTransformCamera";
            this.buttonTransformCamera.Size = new System.Drawing.Size(330, 46);
            this.buttonTransformCamera.TabIndex = 9;
            this.buttonTransformCamera.Text = "Применить аффинные преобразования к\r\nвыделенной камере";
            this.buttonTransformCamera.UseVisualStyleBackColor = true;
            this.buttonTransformCamera.Click += new System.EventHandler(this.buttonTransformCamera_Click);
            // 
            // buttonSetActiveCamera
            // 
            this.buttonSetActiveCamera.Location = new System.Drawing.Point(9, 557);
            this.buttonSetActiveCamera.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonSetActiveCamera.Name = "buttonSetActiveCamera";
            this.buttonSetActiveCamera.Size = new System.Drawing.Size(330, 46);
            this.buttonSetActiveCamera.TabIndex = 8;
            this.buttonSetActiveCamera.Text = "Сделать выделенную камеру активной";
            this.buttonSetActiveCamera.UseVisualStyleBackColor = true;
            this.buttonSetActiveCamera.Click += new System.EventHandler(this.buttonSetActiveCamera_Click);
            // 
            // buttonAddCamera
            // 
            this.buttonAddCamera.Location = new System.Drawing.Point(179, 472);
            this.buttonAddCamera.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonAddCamera.Name = "buttonAddCamera";
            this.buttonAddCamera.Size = new System.Drawing.Size(160, 77);
            this.buttonAddCamera.TabIndex = 7;
            this.buttonAddCamera.Text = "[+] Добавить камеру";
            this.buttonAddCamera.UseVisualStyleBackColor = true;
            this.buttonAddCamera.Click += new System.EventHandler(this.buttonAddCamera_Click);
            // 
            // dataGridViewCameras
            // 
            this.dataGridViewCameras.AllowUserToAddRows = false;
            this.dataGridViewCameras.AllowUserToDeleteRows = false;
            this.dataGridViewCameras.AllowUserToResizeColumns = false;
            this.dataGridViewCameras.AllowUserToResizeRows = false;
            this.dataGridViewCameras.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCameras.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.Column1,
            this.Column2,
            this.Active});
            this.dataGridViewCameras.Location = new System.Drawing.Point(9, 9);
            this.dataGridViewCameras.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dataGridViewCameras.MultiSelect = false;
            this.dataGridViewCameras.Name = "dataGridViewCameras";
            this.dataGridViewCameras.ReadOnly = true;
            this.dataGridViewCameras.RowHeadersVisible = false;
            this.dataGridViewCameras.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewCameras.Size = new System.Drawing.Size(330, 345);
            this.dataGridViewCameras.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "№";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 55;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "X";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 62;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Y";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 63;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Z";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 62;
            // 
            // Active
            // 
            this.Active.HeaderText = "Активная";
            this.Active.Name = "Active";
            this.Active.ReadOnly = true;
            this.Active.Width = 63;
            // 
            // buttonDeleteCamera
            // 
            this.buttonDeleteCamera.Location = new System.Drawing.Point(9, 472);
            this.buttonDeleteCamera.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonDeleteCamera.Name = "buttonDeleteCamera";
            this.buttonDeleteCamera.Size = new System.Drawing.Size(160, 77);
            this.buttonDeleteCamera.TabIndex = 6;
            this.buttonDeleteCamera.Text = "[-] Удалить выделенную камеру";
            this.buttonDeleteCamera.UseVisualStyleBackColor = true;
            this.buttonDeleteCamera.Click += new System.EventHandler(this.buttonDeleteCamera_Click);
            // 
            // buttonConfigCamera
            // 
            this.buttonConfigCamera.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonConfigCamera.Location = new System.Drawing.Point(9, 364);
            this.buttonConfigCamera.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonConfigCamera.Name = "buttonConfigCamera";
            this.buttonConfigCamera.Size = new System.Drawing.Size(330, 46);
            this.buttonConfigCamera.TabIndex = 5;
            this.buttonConfigCamera.Text = "Открыть/изменить свойства выделенной камеры";
            this.buttonConfigCamera.UseVisualStyleBackColor = true;
            this.buttonConfigCamera.Click += new System.EventHandler(this.buttonConfigCamera_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.справкаToolStripMenuItem,
            this.настройкиToolStripMenuItem,
            this.выходИзПрограммыToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(9, 3, 0, 3);
            this.menuStrip1.Size = new System.Drawing.Size(1081, 25);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.оПрограммеToolStripMenuItem,
            this.обАвтореToolStripMenuItem});
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 19);
            this.справкаToolStripMenuItem.Text = "Справка";
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.оПрограммеToolStripMenuItem.Text = "О системе координат";
            this.оПрограммеToolStripMenuItem.Click += new System.EventHandler(this.оСистемеКоординатToolStripMenuItem_Click);
            // 
            // обАвтореToolStripMenuItem
            // 
            this.обАвтореToolStripMenuItem.Name = "обАвтореToolStripMenuItem";
            this.обАвтореToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.обАвтореToolStripMenuItem.Text = "Об авторе";
            this.обАвтореToolStripMenuItem.Click += new System.EventHandler(this.обАвтореToolStripMenuItem_Click);
            // 
            // настройкиToolStripMenuItem
            // 
            this.настройкиToolStripMenuItem.Name = "настройкиToolStripMenuItem";
            this.настройкиToolStripMenuItem.Size = new System.Drawing.Size(117, 19);
            this.настройкиToolStripMenuItem.Text = "Настройки сцены";
            this.настройкиToolStripMenuItem.Click += new System.EventHandler(this.настройкиToolStripMenuItem_Click);
            // 
            // выходИзПрограммыToolStripMenuItem
            // 
            this.выходИзПрограммыToolStripMenuItem.Name = "выходИзПрограммыToolStripMenuItem";
            this.выходИзПрограммыToolStripMenuItem.Size = new System.Drawing.Size(137, 19);
            this.выходИзПрограммыToolStripMenuItem.Text = "Выход из программы";
            this.выходИзПрограммыToolStripMenuItem.Click += new System.EventHandler(this.выходИзПрограммыToolStripMenuItem_Click);
            // 
            // labelNormal
            // 
            this.labelNormal.AutoSize = true;
            this.labelNormal.BackColor = System.Drawing.SystemColors.Control;
            this.labelNormal.Location = new System.Drawing.Point(717, 2);
            this.labelNormal.Name = "labelNormal";
            this.labelNormal.Size = new System.Drawing.Size(0, 20);
            this.labelNormal.TabIndex = 4;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Location = new System.Drawing.Point(0, 25);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(720, 720);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.Resize += new System.EventHandler(this.pictureBox1_Resize);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1081, 746);
            this.Controls.Add(this.labelNormal);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Редактор сцены";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPagePrimitives.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPrimitives)).EndInit();
            this.tabPageLights.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLights)).EndInit();
            this.tabPageCameras.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCameras)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem обАвтореToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem настройкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходИзПрограммыToolStripMenuItem;
        private System.Windows.Forms.DataGridView dataGridViewPrimitives;
        private System.Windows.Forms.Button buttonAddPrimitive;
        private System.Windows.Forms.Button buttonDeletePrimitive;
        private System.Windows.Forms.Button buttonConfigPrimitive;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPagePrimitives;
        private System.Windows.Forms.TabPage tabPageLights;
        private System.Windows.Forms.TabPage tabPageCameras;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrimitiveIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrimitiveType;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrimitiveColor;
        private System.Windows.Forms.Button buttonAddLight;
        private System.Windows.Forms.DataGridView dataGridViewLights;
        private System.Windows.Forms.Button buttonDeleteLight;
        private System.Windows.Forms.Button buttonConfigLight;
        private System.Windows.Forms.Button buttonAddCamera;
        private System.Windows.Forms.DataGridView dataGridViewCameras;
        private System.Windows.Forms.Button buttonDeleteCamera;
        private System.Windows.Forms.Button buttonConfigCamera;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Active;
        private System.Windows.Forms.Button buttonSetActiveCamera;
        private System.Windows.Forms.Button buttonTransformPrimitive;
        private System.Windows.Forms.Button buttonTransformLight;
        private System.Windows.Forms.Button buttonTransformCamera;
        private System.Windows.Forms.Button buttonTrace;
        private System.Windows.Forms.Label labelNormal;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}

