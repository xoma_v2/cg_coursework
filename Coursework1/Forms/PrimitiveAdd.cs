﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Coursework1.Forms
{
    public partial class PrimitiveAdd : Form
    {
        private List<Primitive> primitives;

        public PrimitiveAdd(List<Primitive> primitives)
        {
            InitializeComponent();
            this.primitives = primitives;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            ColorDialog MyDialog = new ColorDialog();
            MyDialog.AllowFullOpen = false;

            if (MyDialog.ShowDialog() == DialogResult.OK)
                pictureBox1.BackColor = MyDialog.Color;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button1.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button2.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button3.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button4.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button5.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button6.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button7.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button8.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button9.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button10.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button11.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button12.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button13.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button14.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button15.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button16.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            float specular = (float)numericUpDownSpecular.Value;
            float reflective = (float)numericUpDownReflective.Value;
            Point3D color = new Point3D(pictureBox1.BackColor.R, pictureBox1.BackColor.G, pictureBox1.BackColor.B);

            if (tabControl1.SelectedTab.Name == "tabPageSphere")
            {
                Point3D center = Point3D.Parse(button1.Text);
                float radius = (float)numericUpDown1.Value;

                if (center != null)
                    primitives.Add(new Sphere(center, radius, color, specular, reflective));
                else
                    MessageBox.Show("Заполните все необходимые поля для добавления примитива.");
            }
            else if (tabControl1.SelectedTab.Name == "tabPageParallelepiped")
            {
                Point3D O = Point3D.Parse(button2.Text);
                Point3D Y = Point3D.Parse(button3.Text);
                Point3D X = Point3D.Parse(button4.Text);
                Point3D Z = Point3D.Parse(button5.Text);

                if (O != null && X != null && Y != null && Z != null)
                    primitives.Add(new Parallelepiped(O, X, Y, Z, color, specular, reflective));
                else
                    MessageBox.Show("Заполните все необходимые поля для добавления примитива.");
            }
            else if (tabControl1.SelectedTab.Name == "tabPageTetrahedralPyramid")
            {
                Point3D A = Point3D.Parse(button6.Text);
                Point3D B = Point3D.Parse(button7.Text);
                Point3D C = Point3D.Parse(button8.Text);
                Point3D D = Point3D.Parse(button9.Text);
                Point3D O = Point3D.Parse(button10.Text);

                if (O != null && A != null && B != null && C != null && D != null)
                    primitives.Add(new TetrahedralPyramid(A, B, C, D, O, color, specular, reflective));
                else
                    MessageBox.Show("Заполните все необходимые поля для добавления примитива.");
            }
            else if (tabControl1.SelectedTab.Name == "tabPageTriangularPrism")
            {
                Point3D O = Point3D.Parse(button11.Text);
                Point3D Y = Point3D.Parse(button12.Text);
                Point3D X = Point3D.Parse(button13.Text);
                Point3D Z = Point3D.Parse(button14.Text);

                if (O != null && X != null && Y != null && Z != null)
                    primitives.Add(new TriangularPrism(O, X, Y, Z, color, specular, reflective));
                else
                    MessageBox.Show("Заполните все необходимые поля для добавления примитива.");
            }
            else if (tabControl1.SelectedTab.Name == "tabPageCone")
            {
                Point3D center = Point3D.Parse(button15.Text);
                float radius = (float)numericUpDown2.Value;
                float height = (float)numericUpDown3.Value;

                if (center != null)
                    primitives.Add(new Cone(center, radius, height, color, specular, reflective));
                else
                    MessageBox.Show("Заполните все необходимые поля для добавления примитива.");
            }
            else if (tabControl1.SelectedTab.Name == "tabPageCylinder")
            {
                Point3D center = Point3D.Parse(button16.Text);
                float radius = (float)numericUpDown4.Value;
                float height = (float)numericUpDown5.Value;

                if (center != null)
                    primitives.Add(new Cylinder(center, radius, height, color, specular, reflective));
                else
                    MessageBox.Show("Заполните все необходимые поля для добавления примитива.");
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
