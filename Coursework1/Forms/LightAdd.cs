﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Coursework1.Forms
{
    public partial class LightAdd : Form
    {
        private List<Light> lights;
        //private List<float> intensitys;

        public LightAdd(List<Light> lights/*, List<float> intensitys*/)
        {
            InitializeComponent();
            this.lights = lights;
            //this.intensitys = intensitys;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button1.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button2.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        /*private float IntensityNormolize(float intensity)
        {
            intensitys.Add(intensity);
            float sum = intensitys.Sum();
            
            for (int i = 0; i < lights.Count; i++)
                lights[i].Intensity = intensitys[i] / sum;

            return intensity / sum;
        }*/

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab.Name == "tabPagePoint")
            {
                Point3D position = Point3D.Parse(button1.Text);
                float intensity = (float)numericUpDown1.Value;

                if (position != null)
                {
                    //lights.Add(new Light(LightType.Point, IntensityNormolize(intensity), position));
                    lights.Add(new Light(LightType.Point, intensity, pictureBox1.BackColor, position));
                }
                else
                    MessageBox.Show("Заполните все необходимые поля для добавления источника света.");
            }
            else if (tabControl1.SelectedTab.Name == "tabPageDirectional")
            {
                Point3D direction = Point3D.Parse(button2.Text);
                float intensity = (float)numericUpDown2.Value;

                if (direction != null)
                    lights.Add(new Light(LightType.Directional, intensity, pictureBox1.BackColor, direction));
                else
                    MessageBox.Show("Заполните все необходимые поля для добавления источника света.");
            }
            else if (tabControl1.SelectedTab.Name == "tabPageAmbient")
            {
                float intensity = (float)numericUpDown3.Value;

                lights.Add(new Light(LightType.Ambient, intensity, pictureBox1.BackColor));
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            ColorDialog MyDialog = new ColorDialog();
            MyDialog.AllowFullOpen = false;

            if (MyDialog.ShowDialog() == DialogResult.OK)
                pictureBox1.BackColor = MyDialog.Color;
        }
    }
}
