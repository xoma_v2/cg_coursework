﻿namespace Coursework1.Forms
{
    partial class Transformations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.buttonMove = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageMove = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.numericUpDown6 = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.tabPageRotation = new System.Windows.Forms.TabPage();
            this.buttonRotate = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.tabPageScaling = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDown9 = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown8 = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.numericUpDown7 = new System.Windows.Forms.NumericUpDown();
            this.buttonScaling = new System.Windows.Forms.Button();
            this.tabPageUniformScaling = new System.Windows.Forms.TabPage();
            this.label16 = new System.Windows.Forms.Label();
            this.numericUpDown10 = new System.Windows.Forms.NumericUpDown();
            this.buttonUniformScaling = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPageMove.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
            this.tabPageRotation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.tabPageScaling.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).BeginInit();
            this.tabPageUniformScaling.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown10)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.buttonAdd);
            this.panel1.Controls.Add(this.radioButton1);
            this.panel1.Controls.Add(this.radioButton2);
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(357, 111);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(237, 127);
            this.panel1.TabIndex = 17;
            // 
            // buttonAdd
            // 
            this.buttonAdd.Enabled = false;
            this.buttonAdd.Location = new System.Drawing.Point(31, 83);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(173, 30);
            this.buttonAdd.TabIndex = 26;
            this.buttonAdd.Text = "Добавить";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(8, 3);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(196, 44);
            this.radioButton1.TabIndex = 8;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Относительно центра\r\nобъекта";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(8, 53);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(190, 24);
            this.radioButton2.TabIndex = 9;
            this.radioButton2.Text = "Относительно точки:";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // buttonMove
            // 
            this.buttonMove.Location = new System.Drawing.Point(6, 219);
            this.buttonMove.Name = "buttonMove";
            this.buttonMove.Size = new System.Drawing.Size(301, 32);
            this.buttonMove.TabIndex = 5;
            this.buttonMove.Text = "Перенос";
            this.buttonMove.UseVisualStyleBackColor = true;
            this.buttonMove.Click += new System.EventHandler(this.buttonMove_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageMove);
            this.tabControl1.Controls.Add(this.tabPageRotation);
            this.tabControl1.Controls.Add(this.tabPageScaling);
            this.tabControl1.Controls.Add(this.tabPageUniformScaling);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(321, 315);
            this.tabControl1.TabIndex = 9;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPageMove
            // 
            this.tabPageMove.Controls.Add(this.label11);
            this.tabPageMove.Controls.Add(this.numericUpDown6);
            this.tabPageMove.Controls.Add(this.label10);
            this.tabPageMove.Controls.Add(this.numericUpDown5);
            this.tabPageMove.Controls.Add(this.label9);
            this.tabPageMove.Controls.Add(this.numericUpDown4);
            this.tabPageMove.Controls.Add(this.buttonMove);
            this.tabPageMove.Location = new System.Drawing.Point(4, 54);
            this.tabPageMove.Name = "tabPageMove";
            this.tabPageMove.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMove.Size = new System.Drawing.Size(313, 257);
            this.tabPageMove.TabIndex = 1;
            this.tabPageMove.Text = "Перенос";
            this.tabPageMove.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(64, 104);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 20);
            this.label11.TabIndex = 21;
            this.label11.Text = "ΔZ:";
            // 
            // numericUpDown6
            // 
            this.numericUpDown6.DecimalPlaces = 2;
            this.numericUpDown6.Location = new System.Drawing.Point(105, 102);
            this.numericUpDown6.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown6.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown6.Name = "numericUpDown6";
            this.numericUpDown6.Size = new System.Drawing.Size(151, 26);
            this.numericUpDown6.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(64, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 20);
            this.label10.TabIndex = 19;
            this.label10.Text = "ΔY:";
            // 
            // numericUpDown5
            // 
            this.numericUpDown5.DecimalPlaces = 2;
            this.numericUpDown5.Location = new System.Drawing.Point(105, 70);
            this.numericUpDown5.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown5.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown5.Name = "numericUpDown5";
            this.numericUpDown5.Size = new System.Drawing.Size(151, 26);
            this.numericUpDown5.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(64, 40);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 20);
            this.label9.TabIndex = 17;
            this.label9.Text = "ΔX:";
            // 
            // numericUpDown4
            // 
            this.numericUpDown4.DecimalPlaces = 2;
            this.numericUpDown4.Location = new System.Drawing.Point(105, 38);
            this.numericUpDown4.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown4.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown4.Name = "numericUpDown4";
            this.numericUpDown4.Size = new System.Drawing.Size(151, 26);
            this.numericUpDown4.TabIndex = 16;
            // 
            // tabPageRotation
            // 
            this.tabPageRotation.Controls.Add(this.buttonRotate);
            this.tabPageRotation.Controls.Add(this.label13);
            this.tabPageRotation.Controls.Add(this.numericUpDown3);
            this.tabPageRotation.Controls.Add(this.label14);
            this.tabPageRotation.Controls.Add(this.numericUpDown2);
            this.tabPageRotation.Controls.Add(this.label15);
            this.tabPageRotation.Controls.Add(this.numericUpDown1);
            this.tabPageRotation.Location = new System.Drawing.Point(4, 54);
            this.tabPageRotation.Name = "tabPageRotation";
            this.tabPageRotation.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRotation.Size = new System.Drawing.Size(313, 257);
            this.tabPageRotation.TabIndex = 0;
            this.tabPageRotation.Text = "Поворот";
            this.tabPageRotation.UseVisualStyleBackColor = true;
            // 
            // buttonRotate
            // 
            this.buttonRotate.Location = new System.Drawing.Point(6, 219);
            this.buttonRotate.Name = "buttonRotate";
            this.buttonRotate.Size = new System.Drawing.Size(301, 32);
            this.buttonRotate.TabIndex = 28;
            this.buttonRotate.Text = "Поворот";
            this.buttonRotate.UseVisualStyleBackColor = true;
            this.buttonRotate.Click += new System.EventHandler(this.buttonRotate_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 104);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(120, 20);
            this.label13.TabIndex = 27;
            this.label13.Text = "Вокруг оси OZ:";
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.DecimalPlaces = 2;
            this.numericUpDown3.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown3.Location = new System.Drawing.Point(144, 102);
            this.numericUpDown3.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numericUpDown3.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(151, 26);
            this.numericUpDown3.TabIndex = 26;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(17, 72);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(121, 20);
            this.label14.TabIndex = 25;
            this.label14.Text = "Вокруг оси OY:";
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.DecimalPlaces = 2;
            this.numericUpDown2.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown2.Location = new System.Drawing.Point(144, 70);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numericUpDown2.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(151, 26);
            this.numericUpDown2.TabIndex = 24;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(17, 40);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(121, 20);
            this.label15.TabIndex = 23;
            this.label15.Text = "Вокруг оси OX:";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.DecimalPlaces = 2;
            this.numericUpDown1.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown1.Location = new System.Drawing.Point(144, 38);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(151, 26);
            this.numericUpDown1.TabIndex = 22;
            // 
            // tabPageScaling
            // 
            this.tabPageScaling.Controls.Add(this.label1);
            this.tabPageScaling.Controls.Add(this.numericUpDown9);
            this.tabPageScaling.Controls.Add(this.label2);
            this.tabPageScaling.Controls.Add(this.numericUpDown8);
            this.tabPageScaling.Controls.Add(this.label12);
            this.tabPageScaling.Controls.Add(this.numericUpDown7);
            this.tabPageScaling.Controls.Add(this.buttonScaling);
            this.tabPageScaling.Location = new System.Drawing.Point(4, 54);
            this.tabPageScaling.Name = "tabPageScaling";
            this.tabPageScaling.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageScaling.Size = new System.Drawing.Size(313, 257);
            this.tabPageScaling.TabIndex = 2;
            this.tabPageScaling.Text = "Масштабирование";
            this.tabPageScaling.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(68, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 20);
            this.label1.TabIndex = 28;
            this.label1.Text = "kZ:";
            // 
            // numericUpDown9
            // 
            this.numericUpDown9.DecimalPlaces = 2;
            this.numericUpDown9.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDown9.Location = new System.Drawing.Point(105, 102);
            this.numericUpDown9.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown9.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            this.numericUpDown9.Name = "numericUpDown9";
            this.numericUpDown9.Size = new System.Drawing.Size(151, 26);
            this.numericUpDown9.TabIndex = 27;
            this.numericUpDown9.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(67, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 20);
            this.label2.TabIndex = 26;
            this.label2.Text = "kY:";
            // 
            // numericUpDown8
            // 
            this.numericUpDown8.DecimalPlaces = 2;
            this.numericUpDown8.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDown8.Location = new System.Drawing.Point(105, 70);
            this.numericUpDown8.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown8.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            this.numericUpDown8.Name = "numericUpDown8";
            this.numericUpDown8.Size = new System.Drawing.Size(151, 26);
            this.numericUpDown8.TabIndex = 25;
            this.numericUpDown8.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(67, 40);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 20);
            this.label12.TabIndex = 24;
            this.label12.Text = "kX:";
            // 
            // numericUpDown7
            // 
            this.numericUpDown7.DecimalPlaces = 2;
            this.numericUpDown7.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDown7.Location = new System.Drawing.Point(105, 38);
            this.numericUpDown7.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown7.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            this.numericUpDown7.Name = "numericUpDown7";
            this.numericUpDown7.Size = new System.Drawing.Size(151, 26);
            this.numericUpDown7.TabIndex = 23;
            this.numericUpDown7.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // buttonScaling
            // 
            this.buttonScaling.Location = new System.Drawing.Point(6, 219);
            this.buttonScaling.Name = "buttonScaling";
            this.buttonScaling.Size = new System.Drawing.Size(301, 32);
            this.buttonScaling.TabIndex = 22;
            this.buttonScaling.Text = "Масштабирование";
            this.buttonScaling.UseVisualStyleBackColor = true;
            this.buttonScaling.Click += new System.EventHandler(this.buttonScaling_Click);
            // 
            // tabPageUniformScaling
            // 
            this.tabPageUniformScaling.Controls.Add(this.label16);
            this.tabPageUniformScaling.Controls.Add(this.numericUpDown10);
            this.tabPageUniformScaling.Controls.Add(this.buttonUniformScaling);
            this.tabPageUniformScaling.Location = new System.Drawing.Point(4, 54);
            this.tabPageUniformScaling.Name = "tabPageUniformScaling";
            this.tabPageUniformScaling.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageUniformScaling.Size = new System.Drawing.Size(313, 257);
            this.tabPageUniformScaling.TabIndex = 3;
            this.tabPageUniformScaling.Text = "Однородное масштабирование";
            this.tabPageUniformScaling.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(78, 40);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(21, 20);
            this.label16.TabIndex = 26;
            this.label16.Text = "k:";
            // 
            // numericUpDown10
            // 
            this.numericUpDown10.DecimalPlaces = 2;
            this.numericUpDown10.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDown10.Location = new System.Drawing.Point(105, 38);
            this.numericUpDown10.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown10.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            this.numericUpDown10.Name = "numericUpDown10";
            this.numericUpDown10.Size = new System.Drawing.Size(151, 26);
            this.numericUpDown10.TabIndex = 25;
            this.numericUpDown10.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // buttonUniformScaling
            // 
            this.buttonUniformScaling.Location = new System.Drawing.Point(6, 219);
            this.buttonUniformScaling.Name = "buttonUniformScaling";
            this.buttonUniformScaling.Size = new System.Drawing.Size(301, 32);
            this.buttonUniformScaling.TabIndex = 23;
            this.buttonUniformScaling.Text = "Однородное масштабирование";
            this.buttonUniformScaling.UseVisualStyleBackColor = true;
            this.buttonUniformScaling.Click += new System.EventHandler(this.buttonUniformScaling_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(357, 285);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(237, 32);
            this.buttonCancel.TabIndex = 29;
            this.buttonCancel.Text = "Закрыть";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(359, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(209, 120);
            this.label3.TabIndex = 30;
            this.label3.Text = "При переносе изменяется\r\nположения камеры.\r\n\r\nПри повороте изменяется\r\nматрица пр" +
    "еобразования\r\nкамеры.";
            this.label3.Visible = false;
            // 
            // Transformations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(613, 340);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label3);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(629, 379);
            this.MinimumSize = new System.Drawing.Size(629, 379);
            this.Name = "Transformations";
            this.Text = "Преобразования";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPageMove.ResumeLayout(false);
            this.tabPageMove.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
            this.tabPageRotation.ResumeLayout(false);
            this.tabPageRotation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.tabPageScaling.ResumeLayout(false);
            this.tabPageScaling.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).EndInit();
            this.tabPageUniformScaling.ResumeLayout(false);
            this.tabPageUniformScaling.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown10)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.Button buttonMove;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageRotation;
        private System.Windows.Forms.TabPage tabPageMove;
        private System.Windows.Forms.TabPage tabPageScaling;
        private System.Windows.Forms.Button buttonRotate;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown numericUpDown6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numericUpDown5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numericUpDown4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDown9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown numericUpDown7;
        private System.Windows.Forms.Button buttonScaling;
        private System.Windows.Forms.TabPage tabPageUniformScaling;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown numericUpDown10;
        private System.Windows.Forms.Button buttonUniformScaling;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label label3;
    }
}