﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Coursework1.Forms
{
    public partial class FormXYZ : Form
    {
        public FormXYZ()
        {
            InitializeComponent();
        }

        public FormXYZ(Point3D point)
        {
            InitializeComponent();
            numericUpDownX.Value = (decimal)point.X;
            numericUpDownY.Value = (decimal)point.Y;
            numericUpDownZ.Value = (decimal)point.Z;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        public Point3D Value
        {
            get
            {
                return new Point3D((float)numericUpDownX.Value,
                                   (float)numericUpDownY.Value,
                                   (float)numericUpDownZ.Value);
            }
        }
    }
}
