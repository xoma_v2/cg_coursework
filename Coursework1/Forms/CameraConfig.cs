﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Coursework1.Forms
{
    public partial class CameraConfig : Form
    {
        public Camera Value { get; private set; }

        public CameraConfig(Camera camera)
        {
            InitializeComponent();
            this.Value = camera;

            numericUpDownX.Value = (decimal)camera.Position.X;
            numericUpDownY.Value = (decimal)camera.Position.Y;
            numericUpDownZ.Value = (decimal)camera.Position.Z;

            numericUpDown11.Value = (decimal)camera.Rotation[0, 0]; numericUpDown12.Value = (decimal)camera.Rotation[0, 1];
            numericUpDown13.Value = (decimal)camera.Rotation[0, 2]; numericUpDown14.Value = (decimal)camera.Rotation[0, 3];

            numericUpDown21.Value = (decimal)camera.Rotation[1, 0]; numericUpDown22.Value = (decimal)camera.Rotation[1, 1];
            numericUpDown23.Value = (decimal)camera.Rotation[1, 2]; numericUpDown24.Value = (decimal)camera.Rotation[1, 3];

            numericUpDown31.Value = (decimal)camera.Rotation[2, 0]; numericUpDown32.Value = (decimal)camera.Rotation[2, 1];
            numericUpDown33.Value = (decimal)camera.Rotation[2, 2]; numericUpDown34.Value = (decimal)camera.Rotation[2, 3];

            numericUpDown41.Value = (decimal)camera.Rotation[3, 0]; numericUpDown42.Value = (decimal)camera.Rotation[3, 1];
            numericUpDown43.Value = (decimal)camera.Rotation[3, 2]; numericUpDown44.Value = (decimal)camera.Rotation[3, 3];
        }

        private void buttonConfig_Click(object sender, EventArgs e)
        {
            Value = new Camera(new Point3D((float)numericUpDownX.Value,
                                           (float)numericUpDownY.Value,
                                           (float)numericUpDownZ.Value),
                               new float[,] { { (float)numericUpDown11.Value, (float)numericUpDown12.Value, (float)numericUpDown13.Value, (float)numericUpDown14.Value },
                                              { (float)numericUpDown21.Value, (float)numericUpDown22.Value, (float)numericUpDown23.Value, (float)numericUpDown24.Value },
                                              { (float)numericUpDown31.Value, (float)numericUpDown32.Value, (float)numericUpDown33.Value, (float)numericUpDown34.Value },
                                              { (float)numericUpDown41.Value, (float)numericUpDown42.Value, (float)numericUpDown43.Value, (float)numericUpDown44.Value } });
        }
    }
}
