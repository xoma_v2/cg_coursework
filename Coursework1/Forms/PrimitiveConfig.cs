﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Coursework1.Forms
{
    public partial class PrimitiveConfig : Form
    {
        public Primitive Value { get; private set; }

        public PrimitiveConfig(Primitive primitive)
        {
            InitializeComponent();
            this.Value = primitive;

            if (primitive.GetType().Name != "Sphere")
                tabControl1.TabPages.Remove(tabPageSphere);
            else
            {
                Sphere obj = (Sphere)primitive;
                button1.Text = obj.Center.X.ToString() + " " + obj.Center.Y.ToString() + " " + obj.Center.Z.ToString();
                numericUpDown1.Value = (decimal)obj.Radius;
            }

            if (primitive.GetType().Name != "Parallelepiped")
                tabControl1.TabPages.Remove(tabPageParallelepiped);
            else
            {
                Parallelepiped obj = (Parallelepiped)primitive;
                button2.Text = obj.A.X.ToString() + " " + obj.A.Y.ToString() + " " + obj.A.Z.ToString();
                button3.Text = obj.B.X.ToString() + " " + obj.B.Y.ToString() + " " + obj.B.Z.ToString();
                button4.Text = obj.D.X.ToString() + " " + obj.D.Y.ToString() + " " + obj.D.Z.ToString();
                button5.Text = obj.A1.X.ToString() + " " + obj.A1.Y.ToString() + " " + obj.A1.Z.ToString();
            }

            if (primitive.GetType().Name != "TetrahedralPyramid")
                tabControl1.TabPages.Remove(tabPageTetrahedralPyramid);
            else
            {
                TetrahedralPyramid obj = (TetrahedralPyramid)primitive;
                button6.Text = obj.a.X.ToString() + " " + obj.a.Y.ToString() + " " + obj.a.Z.ToString();
                button7.Text = obj.b.X.ToString() + " " + obj.b.Y.ToString() + " " + obj.b.Z.ToString();
                button8.Text = obj.c.X.ToString() + " " + obj.c.Y.ToString() + " " + obj.c.Z.ToString();
                button9.Text = obj.d.X.ToString() + " " + obj.d.Y.ToString() + " " + obj.d.Z.ToString();
                button10.Text = obj.o.X.ToString() + " " + obj.o.Y.ToString() + " " + obj.o.Z.ToString();
            }

            if (primitive.GetType().Name != "TriangularPrism")
                tabControl1.TabPages.Remove(tabPageTriangularPrism);
            else
            {
                TriangularPrism obj = (TriangularPrism)primitive;
                button11.Text = obj.A.X.ToString() + " " + obj.A.Y.ToString() + " " + obj.A.Z.ToString();
                button12.Text = obj.B.X.ToString() + " " + obj.B.Y.ToString() + " " + obj.B.Z.ToString();
                button13.Text = obj.C.X.ToString() + " " + obj.C.Y.ToString() + " " + obj.C.Z.ToString();
                button14.Text = obj.A1.X.ToString() + " " + obj.A1.Y.ToString() + " " + obj.A1.Z.ToString();
            }

            if (primitive.GetType().Name != "Cone")
                tabControl1.TabPages.Remove(tabPageCone);
            else
            {
                Cone obj = (Cone)primitive;
                button15.Text = obj.Center.X.ToString() + " " + obj.Center.Y.ToString() + " " + obj.Center.Z.ToString();
                numericUpDown2.Value = (decimal)obj.Radius;
                numericUpDown3.Value = (decimal)obj.Height;
            }

            if (primitive.GetType().Name != "Cylinder")
                tabControl1.TabPages.Remove(tabPageCylinder);
            else
            {
                Cylinder obj = (Cylinder)primitive;
                button16.Text = obj.Center.X.ToString() + " " + obj.Center.Y.ToString() + " " + obj.Center.Z.ToString();
                numericUpDown4.Value = (decimal)obj.Radius;
                numericUpDown5.Value = (decimal)obj.Height;
            }

            pictureBox1.BackColor = primitive.Color.Format24bppRgb;
            numericUpDownSpecular.Value = (decimal)primitive.Specular;
            numericUpDownReflective.Value = (decimal)primitive.Reflective;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            ColorDialog MyDialog = new ColorDialog();
            MyDialog.AllowFullOpen = false;

            if (MyDialog.ShowDialog() == DialogResult.OK)
                pictureBox1.BackColor = MyDialog.Color;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button1.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button2.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button3.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button4.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button5.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button6.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button7.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button8.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button9.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button10.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button11.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button12.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button13.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button14.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button15.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button16.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void buttonConfig_Click(object sender, EventArgs e)
        {
            Point3D Color = new Point3D(pictureBox1.BackColor.R, pictureBox1.BackColor.G, pictureBox1.BackColor.B);
            float Specular = (float)numericUpDownSpecular.Value;
            float Reflective = (float)numericUpDownReflective.Value;

            if (Value.GetType().Name == "Sphere")
            {
                Point3D Center = Point3D.Parse(button1.Text);
                float Radius = (float)numericUpDown1.Value;

                Value = new Sphere(Center, Radius, Color, Specular, Reflective);
            }
            else if (Value.GetType().Name == "Parallelepiped")
            {
                Point3D O = Point3D.Parse(button2.Text);
                Point3D Y = Point3D.Parse(button3.Text);
                Point3D X = Point3D.Parse(button4.Text);
                Point3D Z = Point3D.Parse(button5.Text);

                Value = new Parallelepiped(O, X, Y, Z, Color, Specular, Reflective);
            }
            else if (Value.GetType().Name == "TetrahedralPyramid")
            {
                Point3D A = Point3D.Parse(button6.Text);
                Point3D B = Point3D.Parse(button7.Text);
                Point3D C = Point3D.Parse(button8.Text);
                Point3D D = Point3D.Parse(button9.Text);
                Point3D O = Point3D.Parse(button10.Text);

                Value = new TetrahedralPyramid(A, B, C, D, O, Color, Specular, Reflective);
            }
            else if (Value.GetType().Name == "TriangularPrism")
            {
                Point3D O = Point3D.Parse(button11.Text);
                Point3D Y = Point3D.Parse(button12.Text);
                Point3D X = Point3D.Parse(button13.Text);
                Point3D Z = Point3D.Parse(button14.Text);

                Value = new TriangularPrism(O, X, Y, Z, Color, Specular, Reflective);
            }
            else if (Value.GetType().Name == "Cone")
            {
                Point3D Center = Point3D.Parse(button15.Text);
                float Radius = (float)numericUpDown2.Value;
                float Height = (float)numericUpDown3.Value;

                Value = new Cone(Center, Radius, Height, Color, Specular, Reflective);
            }
            else if (Value.GetType().Name == "Cylinder")
            {
                Point3D Center = Point3D.Parse(button16.Text);
                float Radius = (float)numericUpDown4.Value;
                float Height = (float)numericUpDown5.Value;

                Value = new Cylinder(Center, Radius, Height, Color, Specular, Reflective);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
