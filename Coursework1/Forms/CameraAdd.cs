﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Coursework1.Forms
{
    public partial class CameraAdd : Form
    {
        private List<Camera> cameras;

        public CameraAdd(List<Camera> cameras)
        {
            InitializeComponent();
            this.cameras = cameras;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            cameras.Add(new Camera(new Point3D((float)numericUpDownX.Value,
                                               (float)numericUpDownY.Value,
                                               (float)numericUpDownZ.Value),
                                   new float[,] { { (float)numericUpDown11.Value, (float)numericUpDown12.Value, (float)numericUpDown13.Value, (float)numericUpDown14.Value },
                                                  { (float)numericUpDown21.Value, (float)numericUpDown22.Value, (float)numericUpDown23.Value, (float)numericUpDown24.Value },
                                                  { (float)numericUpDown31.Value, (float)numericUpDown32.Value, (float)numericUpDown33.Value, (float)numericUpDown34.Value },
                                                  { (float)numericUpDown41.Value, (float)numericUpDown42.Value, (float)numericUpDown43.Value, (float)numericUpDown44.Value } }));
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
