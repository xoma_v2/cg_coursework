﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Coursework1.Forms
{
    public partial class LightConfig : Form
    {
        public Light Value { get; private set; }

        public LightConfig(Light light)
        {
            InitializeComponent();
            this.Value = light;

            if (light.LType != LightType.Point)
                tabControl1.TabPages.Remove(tabPagePoint);
            else
            {
                button1.Text = light.Position.X.ToString() + " " + light.Position.Y.ToString() + " " + light.Position.Z.ToString();
                numericUpDown1.Value = (decimal)light.Intensity;
            }

            if (light.LType != LightType.Directional)
                tabControl1.TabPages.Remove(tabPageDirectional);
            else
            {
                button2.Text = light.Position.X.ToString() + " " + light.Position.Y.ToString() + " " + light.Position.Z.ToString();
                numericUpDown2.Value = (decimal)light.Intensity;
            }

            if (light.LType != LightType.Ambient)
                tabControl1.TabPages.Remove(tabPageAmbient);
            else
                numericUpDown3.Value = (decimal)light.Intensity;

            pictureBox1.BackColor = light.color;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button1.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormXYZ form = new FormXYZ();
            form.ShowDialog();
            Point3D result = form.Value;
            button2.Text = result.X.ToString() + " " + result.Y.ToString() + " " + result.Z.ToString();
        }

        private void buttonConfig_Click(object sender, EventArgs e)
        {
            if (Value.LType == LightType.Point)
            {
                Point3D Position = Point3D.Parse(button1.Text);
                float Intensity = (float)numericUpDown1.Value;

                Value = new Light(LightType.Point, Intensity, pictureBox1.BackColor, Position);
            }
            else if (Value.LType == LightType.Directional)
            {
                Point3D Direction = Point3D.Parse(button2.Text);
                float Intensity = (float)numericUpDown2.Value;

                Value = new Light(LightType.Directional, Intensity, pictureBox1.BackColor, Direction);
            }
            else if (Value.LType == LightType.Ambient)
            {
                float Intensity = (float)numericUpDown3.Value;

                Value = new Light(LightType.Ambient, Intensity, pictureBox1.BackColor);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            ColorDialog MyDialog = new ColorDialog();
            MyDialog.AllowFullOpen = false;

            if (MyDialog.ShowDialog() == DialogResult.OK)
                pictureBox1.BackColor = MyDialog.Color;
        }
    }
}
