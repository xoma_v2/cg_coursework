﻿namespace Coursework1.Forms
{
    partial class PrimitiveConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPageTriangularPrism = new System.Windows.Forms.TabPage();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPageCone = new System.Windows.Forms.TabPage();
            this.tabPageCylinder = new System.Windows.Forms.TabPage();
            this.buttonConfig = new System.Windows.Forms.Button();
            this.numericUpDownReflective = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.numericUpDownSpecular = new System.Windows.Forms.NumericUpDown();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageSphere = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPageParallelepiped = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tabPageTetrahedralPyramid = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.button15 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
            this.button16 = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.label26 = new System.Windows.Forms.Label();
            this.tabPageTriangularPrism.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.tabPageCone.SuspendLayout();
            this.tabPageCylinder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownReflective)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpecular)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPageSphere.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.tabPageParallelepiped.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabPageTetrahedralPyramid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
            this.SuspendLayout();
            // 
            // tabPageTriangularPrism
            // 
            this.tabPageTriangularPrism.Controls.Add(this.button14);
            this.tabPageTriangularPrism.Controls.Add(this.button13);
            this.tabPageTriangularPrism.Controls.Add(this.button12);
            this.tabPageTriangularPrism.Controls.Add(this.button11);
            this.tabPageTriangularPrism.Controls.Add(this.label17);
            this.tabPageTriangularPrism.Controls.Add(this.label18);
            this.tabPageTriangularPrism.Controls.Add(this.label19);
            this.tabPageTriangularPrism.Controls.Add(this.label20);
            this.tabPageTriangularPrism.Controls.Add(this.pictureBox4);
            this.tabPageTriangularPrism.Location = new System.Drawing.Point(4, 64);
            this.tabPageTriangularPrism.Name = "tabPageTriangularPrism";
            this.tabPageTriangularPrism.Size = new System.Drawing.Size(492, 305);
            this.tabPageTriangularPrism.TabIndex = 0;
            this.tabPageTriangularPrism.Text = "Треугольная призма";
            this.tabPageTriangularPrism.UseVisualStyleBackColor = true;
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(89, 180);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(173, 30);
            this.button14.TabIndex = 38;
            this.button14.Text = "Добавить";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(89, 130);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(173, 30);
            this.button13.TabIndex = 37;
            this.button13.Text = "Добавить";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(89, 80);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(173, 30);
            this.button12.TabIndex = 34;
            this.button12.Text = "Добавить";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(89, 30);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(173, 30);
            this.button11.TabIndex = 33;
            this.button11.Text = "Добавить";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 185);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(73, 20);
            this.label17.TabIndex = 36;
            this.label17.Text = "Точка O:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(13, 135);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(72, 20);
            this.label18.TabIndex = 35;
            this.label18.Text = "Точка C:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(13, 85);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(72, 20);
            this.label19.TabIndex = 32;
            this.label19.Text = "Точка B:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(13, 35);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(72, 20);
            this.label20.TabIndex = 31;
            this.label20.Text = "Точка A:";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Coursework1.Properties.Resources.Треугольная_призма;
            this.pictureBox4.Location = new System.Drawing.Point(256, 31);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(232, 251);
            this.pictureBox4.TabIndex = 39;
            this.pictureBox4.TabStop = false;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(89, 230);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(173, 30);
            this.button10.TabIndex = 40;
            this.button10.Text = "Добавить";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(89, 180);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(173, 30);
            this.button9.TabIndex = 38;
            this.button9.Text = "Добавить";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(89, 130);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(173, 30);
            this.button8.TabIndex = 37;
            this.button8.Text = "Добавить";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(89, 80);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(173, 30);
            this.button7.TabIndex = 34;
            this.button7.Text = "Добавить";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(89, 30);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(173, 30);
            this.button6.TabIndex = 33;
            this.button6.Text = "Добавить";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(13, 235);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 20);
            this.label16.TabIndex = 39;
            this.label16.Text = "Точка O:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 185);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 20);
            this.label5.TabIndex = 36;
            this.label5.Text = "Точка D:";
            // 
            // tabPageCone
            // 
            this.tabPageCone.Controls.Add(this.label23);
            this.tabPageCone.Controls.Add(this.numericUpDown3);
            this.tabPageCone.Controls.Add(this.button15);
            this.tabPageCone.Controls.Add(this.label21);
            this.tabPageCone.Controls.Add(this.numericUpDown2);
            this.tabPageCone.Controls.Add(this.label22);
            this.tabPageCone.Location = new System.Drawing.Point(4, 64);
            this.tabPageCone.Name = "tabPageCone";
            this.tabPageCone.Size = new System.Drawing.Size(492, 305);
            this.tabPageCone.TabIndex = 0;
            this.tabPageCone.Text = "Конус";
            this.tabPageCone.UseVisualStyleBackColor = true;
            // 
            // tabPageCylinder
            // 
            this.tabPageCylinder.Controls.Add(this.label24);
            this.tabPageCylinder.Controls.Add(this.numericUpDown5);
            this.tabPageCylinder.Controls.Add(this.button16);
            this.tabPageCylinder.Controls.Add(this.label25);
            this.tabPageCylinder.Controls.Add(this.numericUpDown4);
            this.tabPageCylinder.Controls.Add(this.label26);
            this.tabPageCylinder.Location = new System.Drawing.Point(4, 64);
            this.tabPageCylinder.Name = "tabPageCylinder";
            this.tabPageCylinder.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCylinder.Size = new System.Drawing.Size(492, 305);
            this.tabPageCylinder.TabIndex = 3;
            this.tabPageCylinder.Text = "Цилиндр";
            this.tabPageCylinder.UseVisualStyleBackColor = true;
            // 
            // buttonConfig
            // 
            this.buttonConfig.Location = new System.Drawing.Point(100, 380);
            this.buttonConfig.Name = "buttonConfig";
            this.buttonConfig.Size = new System.Drawing.Size(277, 50);
            this.buttonConfig.TabIndex = 22;
            this.buttonConfig.Text = "Изменить";
            this.buttonConfig.UseVisualStyleBackColor = true;
            this.buttonConfig.Click += new System.EventHandler(this.buttonConfig_Click);
            // 
            // numericUpDownReflective
            // 
            this.numericUpDownReflective.DecimalPlaces = 2;
            this.numericUpDownReflective.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownReflective.Location = new System.Drawing.Point(40, 237);
            this.numericUpDownReflective.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownReflective.Name = "numericUpDownReflective";
            this.numericUpDownReflective.Size = new System.Drawing.Size(151, 26);
            this.numericUpDownReflective.TabIndex = 17;
            this.numericUpDownReflective.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 134);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(171, 20);
            this.label9.TabIndex = 13;
            this.label9.Text = "Зеркальность (0; +∞):";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 168);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 20);
            this.label7.TabIndex = 15;
            this.label7.Text = "S";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 20);
            this.label8.TabIndex = 12;
            this.label8.Text = "Цвет:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 239);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(21, 20);
            this.label10.TabIndex = 18;
            this.label10.Text = "R";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(14, 205);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(165, 20);
            this.label11.TabIndex = 16;
            this.label11.Text = "Отражаемость [0; 1]:";
            // 
            // numericUpDownSpecular
            // 
            this.numericUpDownSpecular.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericUpDownSpecular.Location = new System.Drawing.Point(40, 166);
            this.numericUpDownSpecular.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownSpecular.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownSpecular.Name = "numericUpDownSpecular";
            this.numericUpDownSpecular.Size = new System.Drawing.Size(151, 26);
            this.numericUpDownSpecular.TabIndex = 9;
            this.numericUpDownSpecular.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Cyan;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(18, 91);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(173, 30);
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(13, 135);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 20);
            this.label13.TabIndex = 35;
            this.label13.Text = "Точка C:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageSphere);
            this.tabControl1.Controls.Add(this.tabPageParallelepiped);
            this.tabControl1.Controls.Add(this.tabPageTetrahedralPyramid);
            this.tabControl1.Controls.Add(this.tabPageTriangularPrism);
            this.tabControl1.Controls.Add(this.tabPageCone);
            this.tabControl1.Controls.Add(this.tabPageCylinder);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabControl1.ItemSize = new System.Drawing.Size(45, 30);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(500, 373);
            this.tabControl1.TabIndex = 21;
            // 
            // tabPageSphere
            // 
            this.tabPageSphere.Controls.Add(this.button1);
            this.tabPageSphere.Controls.Add(this.label6);
            this.tabPageSphere.Controls.Add(this.numericUpDown1);
            this.tabPageSphere.Controls.Add(this.label1);
            this.tabPageSphere.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabPageSphere.Location = new System.Drawing.Point(4, 64);
            this.tabPageSphere.Name = "tabPageSphere";
            this.tabPageSphere.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSphere.Size = new System.Drawing.Size(492, 305);
            this.tabPageSphere.TabIndex = 0;
            this.tabPageSphere.Text = "Сфера";
            this.tabPageSphere.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(17, 58);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(173, 30);
            this.button1.TabIndex = 30;
            this.button1.Text = "Добавить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 105);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 20);
            this.label6.TabIndex = 8;
            this.label6.Text = "Радиус сферы:";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.DecimalPlaces = 2;
            this.numericUpDown1.Location = new System.Drawing.Point(17, 128);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(173, 26);
            this.numericUpDown1.TabIndex = 4;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Центр сферы:";
            // 
            // tabPageParallelepiped
            // 
            this.tabPageParallelepiped.Controls.Add(this.button3);
            this.tabPageParallelepiped.Controls.Add(this.button4);
            this.tabPageParallelepiped.Controls.Add(this.button5);
            this.tabPageParallelepiped.Controls.Add(this.label2);
            this.tabPageParallelepiped.Controls.Add(this.label12);
            this.tabPageParallelepiped.Controls.Add(this.button2);
            this.tabPageParallelepiped.Controls.Add(this.label3);
            this.tabPageParallelepiped.Controls.Add(this.label4);
            this.tabPageParallelepiped.Controls.Add(this.pictureBox2);
            this.tabPageParallelepiped.Location = new System.Drawing.Point(4, 64);
            this.tabPageParallelepiped.Name = "tabPageParallelepiped";
            this.tabPageParallelepiped.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageParallelepiped.Size = new System.Drawing.Size(492, 305);
            this.tabPageParallelepiped.TabIndex = 1;
            this.tabPageParallelepiped.Text = "Параллелепипед";
            this.tabPageParallelepiped.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(89, 80);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(173, 30);
            this.button3.TabIndex = 26;
            this.button3.Text = "Добавить";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(89, 130);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(173, 30);
            this.button4.TabIndex = 29;
            this.button4.Text = "Добавить";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(89, 180);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(173, 30);
            this.button5.TabIndex = 30;
            this.button5.Text = "Добавить";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 185);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 20);
            this.label2.TabIndex = 28;
            this.label2.Text = "Точка O:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 135);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 20);
            this.label12.TabIndex = 27;
            this.label12.Text = "Точка D:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(89, 30);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(173, 30);
            this.button2.TabIndex = 25;
            this.button2.Text = "Добавить";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 20);
            this.label3.TabIndex = 23;
            this.label3.Text = "Точка B:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 20);
            this.label4.TabIndex = 21;
            this.label4.Text = "Точка A:";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Coursework1.Properties.Resources.Параллелепипед;
            this.pictureBox2.Location = new System.Drawing.Point(256, 66);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(245, 191);
            this.pictureBox2.TabIndex = 31;
            this.pictureBox2.TabStop = false;
            // 
            // tabPageTetrahedralPyramid
            // 
            this.tabPageTetrahedralPyramid.Controls.Add(this.button10);
            this.tabPageTetrahedralPyramid.Controls.Add(this.button9);
            this.tabPageTetrahedralPyramid.Controls.Add(this.button8);
            this.tabPageTetrahedralPyramid.Controls.Add(this.button7);
            this.tabPageTetrahedralPyramid.Controls.Add(this.button6);
            this.tabPageTetrahedralPyramid.Controls.Add(this.label16);
            this.tabPageTetrahedralPyramid.Controls.Add(this.label5);
            this.tabPageTetrahedralPyramid.Controls.Add(this.label13);
            this.tabPageTetrahedralPyramid.Controls.Add(this.label14);
            this.tabPageTetrahedralPyramid.Controls.Add(this.label15);
            this.tabPageTetrahedralPyramid.Controls.Add(this.pictureBox3);
            this.tabPageTetrahedralPyramid.Location = new System.Drawing.Point(4, 64);
            this.tabPageTetrahedralPyramid.Name = "tabPageTetrahedralPyramid";
            this.tabPageTetrahedralPyramid.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTetrahedralPyramid.Size = new System.Drawing.Size(492, 305);
            this.tabPageTetrahedralPyramid.TabIndex = 2;
            this.tabPageTetrahedralPyramid.Text = "Четырёхугольная пирамида";
            this.tabPageTetrahedralPyramid.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(13, 85);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(72, 20);
            this.label14.TabIndex = 32;
            this.label14.Text = "Точка B:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(13, 35);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 20);
            this.label15.TabIndex = 31;
            this.label15.Text = "Точка A:";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Coursework1.Properties.Resources.Четырёхугольная_пирамида;
            this.pictureBox3.Location = new System.Drawing.Point(258, 62);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(247, 205);
            this.pictureBox3.TabIndex = 41;
            this.pictureBox3.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.numericUpDownReflective);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.numericUpDownSpecular);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(516, 14);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(213, 344);
            this.panel1.TabIndex = 23;
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(387, 380);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(277, 50);
            this.buttonCancel.TabIndex = 24;
            this.buttonCancel.Text = "Закрыть";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(13, 172);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(124, 20);
            this.label23.TabIndex = 42;
            this.label23.Text = "Высота конуса:";
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.DecimalPlaces = 2;
            this.numericUpDown3.Location = new System.Drawing.Point(17, 195);
            this.numericUpDown3.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(173, 26);
            this.numericUpDown3.TabIndex = 41;
            this.numericUpDown3.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(17, 58);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(173, 30);
            this.button15.TabIndex = 40;
            this.button15.Text = "Добавить";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(13, 105);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(205, 20);
            this.label21.TabIndex = 39;
            this.label21.Text = "Радиус основания конуса:";
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.DecimalPlaces = 2;
            this.numericUpDown2.Location = new System.Drawing.Point(17, 128);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(173, 26);
            this.numericUpDown2.TabIndex = 38;
            this.numericUpDown2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(13, 35);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(199, 20);
            this.label22.TabIndex = 37;
            this.label22.Text = "Центр основания конуса:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(13, 172);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(149, 20);
            this.label24.TabIndex = 48;
            this.label24.Text = "Высота цилиндра:";
            // 
            // numericUpDown5
            // 
            this.numericUpDown5.DecimalPlaces = 2;
            this.numericUpDown5.Location = new System.Drawing.Point(17, 195);
            this.numericUpDown5.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown5.Name = "numericUpDown5";
            this.numericUpDown5.Size = new System.Drawing.Size(173, 26);
            this.numericUpDown5.TabIndex = 47;
            this.numericUpDown5.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(17, 58);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(173, 30);
            this.button16.TabIndex = 46;
            this.button16.Text = "Добавить";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(13, 105);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(226, 20);
            this.label25.TabIndex = 45;
            this.label25.Text = "Радиус основания цилиндра";
            // 
            // numericUpDown4
            // 
            this.numericUpDown4.DecimalPlaces = 2;
            this.numericUpDown4.Location = new System.Drawing.Point(17, 128);
            this.numericUpDown4.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown4.Name = "numericUpDown4";
            this.numericUpDown4.Size = new System.Drawing.Size(173, 26);
            this.numericUpDown4.TabIndex = 44;
            this.numericUpDown4.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(13, 35);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(291, 20);
            this.label26.TabIndex = 43;
            this.label26.Text = "Центр нижнего основания цилиндра:";
            // 
            // PrimitiveConfig
            // 
            this.AcceptButton = this.buttonConfig;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(748, 441);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonConfig);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximumSize = new System.Drawing.Size(764, 480);
            this.MinimumSize = new System.Drawing.Size(764, 480);
            this.Name = "PrimitiveConfig";
            this.Text = "Изменение примитивов";
            this.tabPageTriangularPrism.ResumeLayout(false);
            this.tabPageTriangularPrism.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.tabPageCone.ResumeLayout(false);
            this.tabPageCone.PerformLayout();
            this.tabPageCylinder.ResumeLayout(false);
            this.tabPageCylinder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownReflective)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpecular)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPageSphere.ResumeLayout(false);
            this.tabPageSphere.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.tabPageParallelepiped.ResumeLayout(false);
            this.tabPageParallelepiped.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabPageTetrahedralPyramid.ResumeLayout(false);
            this.tabPageTetrahedralPyramid.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPageTriangularPrism;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPageCone;
        private System.Windows.Forms.TabPage tabPageCylinder;
        private System.Windows.Forms.Button buttonConfig;
        private System.Windows.Forms.NumericUpDown numericUpDownReflective;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown numericUpDownSpecular;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageSphere;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPageParallelepiped;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TabPage tabPageTetrahedralPyramid;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.NumericUpDown numericUpDown5;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.NumericUpDown numericUpDown4;
        private System.Windows.Forms.Label label26;
    }
}