﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Coursework1.Forms
{
    public partial class SceneSettings : Form
    {
        private RayTracer rt;

        public SceneSettings(RayTracer rt)
        {
            InitializeComponent();
            this.rt = rt;
            numericUpDownRecursion.Value = rt.RecursionDepth;
            pictureBox1.BackColor = rt.backgroundColor.Format24bppRgb;
            numericUpDownAngle.Value = rt.Angle;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            ColorDialog MyDialog = new ColorDialog();
            MyDialog.AllowFullOpen = false;

            if (MyDialog.ShowDialog() == DialogResult.OK)
                pictureBox1.BackColor = MyDialog.Color;
        }

        private void buttonConfig_Click(object sender, EventArgs e)
        {
            rt.RecursionDepth = (int)numericUpDownRecursion.Value;
            rt.backgroundColor = new Point3D(pictureBox1.BackColor.R, pictureBox1.BackColor.G, pictureBox1.BackColor.B);
            rt.Angle = (int)numericUpDownAngle.Value;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
