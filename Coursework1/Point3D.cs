﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Coursework1
{
    public class Point3D
    {
        public float X;
        public float Y;
        public float Z;
        public float M;

        public Point3D(float x, float y, float z, float m = 1)
        {
            X = x;
            Y = y;
            Z = z;
            M = m;
        }

        public Point3D(Color color)
        {
            X = color.R;
            Y = color.G;
            Z = color.B;
            M = 1;
        }

        public Color Format24bppRgb
        {
            get
            {
                return Color.FromArgb(Math.Min(255, Math.Max(0, (int)(X + 0.5f))),
                                      Math.Min(255, Math.Max(0, (int)(Y + 0.5f))),
                                      Math.Min(255, Math.Max(0, (int)(Z + 0.5f))));
            }
        }

        public Point3D Move(float dx, float dy, float dz)
        {
            float[,] matrA = new float[,] { { this.X, this.Y, this.Z, 1 } };
            float[,] matrB = new float[,] { { 1,  0,  0,  0 },
                                            { 0,  1,  0,  0 },
                                            { 0,  0,  1,  0 },
                                            { dx, dy, dz, 1 } };

            matrA = MyMath.MultiplyMM(matrA, matrB);

            return new Point3D(matrA[0, 0], matrA[0, 1], matrA[0, 2]);
        }

        public Point3D Scale(float kx, float ky, float kz, Point3D center)
        {
            float[,] matrA = new float[,] { { this.X, this.Y, this.Z, 1 } };
            float[,] matrB = new float[,] { { kx, 0,  0,  0 },
                                            { 0,  ky, 0,  0 },
                                            { 0,  0,  kz, 0 },
                                            { 0,  0,  0,  1 } };
            float[,] matrTo0 = new float[,] { { 1, 0, 0, 0 },
                                              { 0, 1, 0, 0 },
                                              { 0, 0, 1, 0 },
                                              { -center.X, -center.Y, -center.Z, 1 } };
            float[,] matrFrom0 = new float[,] { { 1, 0, 0, 0 },
                                                { 0, 1, 0, 0 },
                                                { 0, 0, 1, 0 },
                                                { center.X, center.Y, center.Z, 1 } };

            matrB = MyMath.MultiplyMM(matrTo0, matrB);
            matrFrom0 = MyMath.MultiplyMM(matrB, matrFrom0);
            matrA = MyMath.MultiplyMM(matrA, matrFrom0);

            return new Point3D(matrA[0, 0], matrA[0, 1], matrA[0, 2]);
        }

        public Point3D Scale(float k, Point3D center)
        {
            float[,] matrA = new float[,] { { this.X, this.Y, this.Z, 1 } };
            float[,] matrB = new float[,] { { k, 0, 0, 0 },
                                            { 0, k, 0, 0 },
                                            { 0, 0, k, 0 },
                                            { 0, 0, 0, 1 } };
            float[,] matrTo0 = new float[,] { { 1, 0, 0, 0 },
                                              { 0, 1, 0, 0 },
                                              { 0, 0, 1, 0 },
                                              { -center.X, -center.Y, -center.Z, 1 } };
            float[,] matrFrom0 = new float[,] { { 1, 0, 0, 0 },
                                                { 0, 1, 0, 0 },
                                                { 0, 0, 1, 0 },
                                                { center.X, center.Y, center.Z, 1 } };

            matrB = MyMath.MultiplyMM(matrTo0, matrB);
            matrFrom0 = MyMath.MultiplyMM(matrB, matrFrom0);
            matrA = MyMath.MultiplyMM(matrA, matrFrom0);

            return new Point3D(matrA[0, 0], matrA[0, 1], matrA[0, 2]);
        }

        public Point3D Rotate(float ax, float ay, float az, Point3D center)
        {
            ax = (float)(ax * Math.PI / 180);
            ay = (float)(ay * Math.PI / 180);
            az = (float)(az * Math.PI / 180);

            float[,] matrA = new float[,] { { this.X, this.Y, this.Z, 1 } };
            float[,] matrBX = new float[,] { { 1, 0,                    0,                   0 },
                                             { 0, (float)Math.Cos(ax),  (float)Math.Sin(ax), 0 },
                                             { 0, -(float)Math.Sin(ax), (float)Math.Cos(ax), 0 },
                                             { 0, 0,                    0,                   1 } };
            float[,] matrBY = new float[,] { { (float)Math.Cos(ay),  0, (float)Math.Sin(ay), 0 },
                                             { 0,                    1, 0,                   0 },
                                             { -(float)Math.Sin(ay), 0, (float)Math.Cos(ay), 0 },
                                             { 0,                    0, 0,                   1 } };
            float[,] matrBZ = new float[,] { { (float)Math.Cos(az),  (float)Math.Sin(az), 0, 0 },
                                             { -(float)Math.Sin(az), (float)Math.Cos(az), 0, 0 },
                                             { 0,                    0,                   1, 0 },
                                             { 0,                    0,                   0, 1 } };
            float[,] matrTo0 = new float[,] { { 1, 0, 0, 0 },
                                              { 0, 1, 0, 0 },
                                              { 0, 0, 1, 0 },
                                              { -center.X, -center.Y, -center.Z, 1 } };
            float[,] matrFrom0 = new float[,] { { 1, 0, 0, 0 },
                                                { 0, 1, 0, 0 },
                                                { 0, 0, 1, 0 },
                                                { center.X, center.Y, center.Z, 1 } };

            matrBX = MyMath.MultiplyMM(matrTo0, matrBX);
            matrBY = MyMath.MultiplyMM(matrBX, matrBY);
            matrBZ = MyMath.MultiplyMM(matrBY, matrBZ);
            matrFrom0 = MyMath.MultiplyMM(matrBZ, matrFrom0);
            matrA = MyMath.MultiplyMM(matrA, matrFrom0);

            return new Point3D(matrA[0, 0], matrA[0, 1], matrA[0, 2]);
        }

        // Преобразует строку формата "0.1 2 -3.4" в объект Point3D
        public static Point3D Parse(string text)
        {
            string[] numbers = text.Split(' ');
            Point3D point;

            try
            {
                point = new Point3D(float.Parse(numbers[0]),
                                    float.Parse(numbers[1]),
                                    float.Parse(numbers[2]));
            }
            catch
            {
                point = null;
            }

            return point;
        }

        // Индексатор
        public float this[int i]
        {
            get
            {
                if (i == 0)
                    return X;
                else if (i == 1)
                    return Y;
                else if (i == 2)
                    return Z;
                else if (i == 3)
                    return M;
                else
                    throw new IndexOutOfRangeException("The collection can hold only 3 elements.");
            }
            set
            {
                if (i == 0)
                    X = value;
                else if (i == 1)
                    Y = value;
                else if (i == 2)
                    Z = value;
                else if (i == 3)
                    M = value;
                else
                    throw new IndexOutOfRangeException("The collection can hold only 3 elements.");
            }
        }
    }
}
