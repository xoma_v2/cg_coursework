﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework1
{
    public static class MyMath
    {
        // Conceptually, an "infinitesimaly small" real number.
        public const float Epsilon = 0.0001f;

        // Скалярное произведение двух 3D векторов
        public static float DotProduct(Point3D vector1, Point3D vector2)
        {
            return vector1.X * vector2.X + vector1.Y * vector2.Y + vector1.Z * vector2.Z;
        }

        // Векторное произведение двух 3D векторов
        public static Point3D CrossProduct(Point3D vector1, Point3D vector2)
        {
            return new Point3D(vector1.Y * vector2.Z - vector1.Z * vector2.Y,
                               vector1.Z * vector2.X - vector1.X * vector2.Z,
                               vector1.X * vector2.Y - vector1.Y * vector2.X);
        }

        // Длина 3D вектора
        public static float Length(Point3D vector)
        {
            return (float)Math.Sqrt(DotProduct(vector, vector));
        }

        // Multiplies a scalar and a vector
        public static Point3D MultiplySV(float k, Point3D vector)
        {
            return new Point3D(k * vector.X, k * vector.Y, k * vector.Z);
        }

        public static Point3D MultiplySV(float[] k, Point3D vector)
        {
            return new Point3D(k[0] * vector.X, k[1] * vector.Y, k[2] * vector.Z);
        }

        // Multiplies a matrix and a vector
        public static Point3D MultiplyMV(float[,] matrix, Point3D vector)
        {
            Point3D result = new Point3D(0, 0, 0, 0);

            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                    result[i] += vector[j] * matrix[i, j];

            return result;
        }
        
        // Функция перемножение матриц
        public static float[,] MultiplyMM(float[,] A, float[,] B)
        {
            int aLenN = A.GetLength(0);
            int aLenM = A.GetLength(1);
            int bLenN = B.GetLength(0);
            int bLenM = B.GetLength(1);

            if (aLenM != bLenN)
                throw new ArgumentException("Не совпадают размерности матриц");

            float[,] C = new float[aLenN, bLenM];

            for (int i = 0; i < aLenN; ++i)
                for (int j = 0; j < bLenM; ++j)
                {
                    C[i, j] = 0;

                    for (int k = 0; k < aLenM; ++k)
                        C[i, j] += A[i, k] * B[k, j];
                }

            return C;
        }

        // Computes v1 + v2
        public static Point3D Add(Point3D vector1, Point3D vector2)
        {
            return new Point3D(vector1.X + vector2.X, vector1.Y + vector2.Y, vector1.Z + vector2.Z);
        }

        // Computes v1 + v2 + ...
        public static Point3D Add(params Point3D[] vectors)
        {
            Point3D result = new Point3D(0, 0, 0);

            foreach (var vector in vectors)
                result = Add(result, vector);

            return result;
        }

        // Computes v1 - v2
        public static Point3D Subtract(Point3D vector1, Point3D vector2)
        {
            return new Point3D(vector1.X - vector2.X, vector1.Y - vector2.Y, vector1.Z - vector2.Z);
        }

        // Computes the reflection of vector respect to normal
        public static Point3D ReflectRay(Point3D vector, Point3D normal)
        {
            return Subtract(MultiplySV(2 * DotProduct(vector, normal), normal), vector);
        }

        // Проверка на ориентацию системы координат
        public static bool IsItRightCoordinateSystem(Point3D i, Point3D j, Point3D k)
        {
            if (DotProduct(CrossProduct(i, j), k) >= 0)
                return true;
            else
                return false;
        }

        // Поиск тройки вектор, образующей правую систему координат
        public static Point3D[] FindRightCoordinateSystem(Point3D i, Point3D j, Point3D k)
        {
            if (DotProduct(CrossProduct(i, j), k) > 0)
                return new Point3D[] { i, j, k };
            else if (DotProduct(CrossProduct(j, i), k) > 0)
                return new Point3D[] { j, i, k };
            else if (DotProduct(CrossProduct(i, k), j) > 0)
                return new Point3D[] { i, k, j };
            else if (DotProduct(CrossProduct(k, i), j) > 0)
                return new Point3D[] { k, i, j };
            else if (DotProduct(CrossProduct(j, k), i) > 0)
                return new Point3D[] { j, k, i };
            else //if (DotProduct(CrossProduct(k, j), i) > 0)
                return new Point3D[] { k, j, i };
        }

        public static Point3D Normalize(Point3D vector)
        {
            return MultiplySV(1.0f / Length(vector), vector);
        }

        public static Point3D Normalize(Point3D v1, Point3D v2)
        {
            Point3D vector = Add(v1, v2);
            return MultiplySV(1.0f / Length(vector), vector);
        }

        public static Point3D Normalize(Point3D v1, Point3D v2, Point3D v3)
        {
            Point3D vector = Add(v1, Add(v2, v3));
            return MultiplySV(1f / Length(vector), vector);
        }

        public static Point3D FindAverage(params Point3D[] points)
        {
            if (points.Length == 0)
                throw new ArgumentException("Для поиска среднего нужен минимум один объект");

            Point3D Max = new Point3D(float.MinValue, float.MinValue, float.MinValue);
            Point3D Min = new Point3D(float.MaxValue, float.MaxValue, float.MaxValue);

            foreach (var point in points)
            {
                if (point.X > Max.X)
                    Max.X = point.X;

                if (point.X < Min.X)
                    Min.X = point.X;

                if (point.Y > Max.Y)
                    Max.Y = point.Y;

                if (point.Y < Min.Y)
                    Min.Y = point.Y;

                if (point.Z > Max.Z)
                    Max.Z = point.Z;

                if (point.Z < Min.Z)
                    Min.Z = point.Z;
            }

            return new Point3D((Min.X + Max.X) / 2f, (Min.Y + Max.Y) / 2f, (Min.Z + Max.Z) / 2f);
        }
    }
}
