﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using System.Drawing;
using System.IO;

namespace Coursework1
{
    public class RayTracer
    {
        private int height;
        private int width;
        private Bitmap canvas;
        public Bitmap Canvas
        {
            get { return canvas; }
            set { canvas = value; height = canvas.Height; width = canvas.Width; }
        }

        private BitmapData canvasData;
        public Point3D backgroundColor { get; set; }
        private float viewportSize;
        private int angle;
        public int Angle
        {
            get { return angle; }
            set
            {
                angle = value;
                //viewportSize = (float)Math.Tan(160 * Math.PI / 180 / 2) * 2;
                viewportSize = (float)Math.Tan(angle * Math.PI / 360) * 2;
            }
        }
        private const float projectionPlaneZ = -1;
        public int RecursionDepth { get; set; }

        public List<Primitive> primitives = new List<Primitive>();
        public List<Light> lights = new List<Light>();
        public List<Camera> cameras = new List<Camera>();

        public RayTracer(Bitmap bitmap, Color background, int recursionDepth = 3, int angle = 120)
        {
            Canvas = bitmap;
            backgroundColor = new Point3D(background.R, background.G, background.B);
            RecursionDepth = recursionDepth;
            Angle = angle;
            primitives.Add(new TetrahedralPyramid(
                new Point3D(-2, 0, -2), new Point3D(0, 0, -4), new Point3D(2, 0, -2), new Point3D(0, 0, 0), new Point3D(0, 3, -2),
                new Point3D(Color.SpringGreen), 10, 0));
            primitives.Add(new TetrahedralPyramid(
                new Point3D(-1, 2, -1), new Point3D(-1, 2, -3), new Point3D(1, 2, -3), new Point3D(1, 2, -1), new Point3D(0, 5, -2),
                new Point3D(Color.SpringGreen), 10, 0));
            primitives.Add(new TetrahedralPyramid(
                new Point3D(-1, 4, -2), new Point3D(0, 4, -3), new Point3D(1, 4, -2), new Point3D(0, 4, -1), new Point3D(0, 7, -2),
                new Point3D(Color.SpringGreen), 10, 0));
            /*
            primitives.Add(new Sphere(new Point3D(3, 0, -2), 1, new Point3D(255, 0, 0), 500, 0.2f));
            primitives.Add(new Sphere(new Point3D(-2, 0, -4), 1, new Point3D(0, 255, 0), 10, 0.3f));
            primitives.Add(new Sphere(new Point3D(2, 0, -4), 1, new Point3D(0, 0, 255), 500, 0.4f));
            primitives.Add(new Sphere(new Point3D(0, -5001, 0), 5000, new Point3D(255, 255, 0), 1000, 0.5f));
            primitives.Add(new Parallelepiped(new Point3D(-2, 0, -3), new Point3D(-1, 0, -4), new Point3D(-3, 0, -4), new Point3D(-2, 2, -3), new Point3D(0, 255, 0), 500, 0.2f));
            primitives.Add(new TetrahedralPyramid(new Point3D(-1, -1, -2), new Point3D(-2, -1, -3.5f), new Point3D(-1.5f, -1, -4), new Point3D(0, -1, -3.5f), new Point3D(-1, 2, -2), new Point3D(0, 255, 255), 500, 0.2f));
            */
            //primitives.Add(new Sphere(new Point3D(0, 0.7f, -2), 1, new Point3D(255, 0, 0), 1000, 0.5f));
            //primitives.Add(new Parallelepiped(new Point3D(0, -2.5f, -2), new Point3D(1, -2.5f, -3), new Point3D(-1, -2.5f, -3), new Point3D(0, -0.5f, -2), new Point3D(0, 255, 255), 1000, 0.2f));
            //primitives.Add(new TriangularPrism(new Point3D(0, -2.5f, -2), new Point3D(1, -2.5f, -3), new Point3D(-1, -2.5f, -3), new Point3D(0, -0.5f, -2), new Point3D(0, 255, 255), 1000, 0.2f));
            //primitives.Add(new TriangularPrism(new Point3D(-1, -1, -2), new Point3D(-2, -1, -3.5f), new Point3D(0, -1, -3.5f), new Point3D(-1, 2, -2), new Point3D(0, 255, 255), 500, 0.2f));
            //primitives.Add(new TriangularPrism(new Point3D(0, -1, -2), new Point3D(1, -1, -3), new Point3D(-1, -1, -3), new Point3D(0, 2, -2), new Point3D(0, 255, 255), 500, 0.2f));
            //primitives.Add(new TriangularPrism(new Point3D(0, -2.5f, -2), new Point3D(1, -2.5f, -3), new Point3D(-1, -2.5f, -3), new Point3D(0, -0.5f, -2), new Point3D(0, 255, 255), 1000, 0.2f));
            /*
            primitives.Add(new Cylinder(new Point3D(0.5f, -3.5f, -2.5f), 1, 3, new Point3D(0, 255, 255), 500, 0.2f));
            */
            //primitives.Add(new Cone(new Point3D(0, -1, -4), 1, 2, new Point3D(0, 255, 255), 500, 0.2f));

            // Суммарная яркость источников освещения не превышает 1.0
            lights.Add(new Light(LightType.Ambient, 0.4f, Color.White));
            //lights.Add(new Light(LightType.Point, 0.6, Color.White, new Point3D(2, 1, 0)));
            lights.Add(new Light(LightType.Point, 0.2f, Color.White, new Point3D(2, 1, 0)));
            lights.Add(new Light(LightType.Point, 0.2f, Color.White, new Point3D(-2, 3, -1)));
            lights.Add(new Light(LightType.Directional, 0.2f, Color.White, new Point3D(1, 4, -4)));

            cameras.Add(new Camera(new Point3D(-2, 2, 3), new float[4, 4] { { 0.97f, 0, -0.26f, 0 },
                                                                            { 0,     1, 0,      0 },
                                                                            { 0.26f, 0, 0.97f,  0 },
                                                                            { 0,     0, 0,      1 } }));
            cameras.Add(new Camera(new Point3D(0, 0, 0)));
            cameras.Add(new Camera(new Point3D(1, 2, -7), new float[4, 4] { { -0.98f, -0.02f, 0.18f,  0 },
                                                                            { 0.02f,  0.97f,  0.22f,  0 },
                                                                            { -0.18f, 0.22f,  -0.96f, 0 },
                                                                            { 0,      0,      0,      1 } }));
        }

        public RayTracer(Bitmap bitmap, Point3D background, int recursionDepth = 3)
        {
            Canvas = bitmap;
            backgroundColor = background;
            RecursionDepth = recursionDepth;
        }

        // The PutPixel() function
        private void PutPixel(float x, float y, Color color)
        {
            int X = (int)(width / 2f + x + 0.5f);
            int Y = (int)(height / 2f - y - 1f + 0.5f);

            if (X >= 0 && X < width && Y >= 0 && Y < height)
                unsafe
                {
                    byte* ptr = (byte*)canvasData.Scan0;
                    ptr[X * 3 + Y * canvasData.Stride] = color.B;
                    ptr[X * 3 + Y * canvasData.Stride + 1] = color.G;
                    ptr[X * 3 + Y * canvasData.Stride + 2] = color.R;
                }
        }

        // Converts 2D canvas coordinates to 3D viewport coordinates
        private Point3D CanvasToViewport(float x, float y)
        {
            return new Point3D(x * viewportSize / width,
                               y * viewportSize / height,
                               projectionPlaneZ);
        }

        // Считаем интенсивность от каждого источника света
        private float[] ComputeLighting(Point3D point, Point3D normal, Point3D view, float specular)
        {
            float[] intensitys = new float[3] { 0, 0, 0 };
            float lengthN = MyMath.Length(normal);
            float lengthV = MyMath.Length(view);

            foreach (Light light in lights)
            {
                float[] intensitysRGB = light.Intensitys;

                if (light.LType == LightType.Ambient)
                {
                    intensitys[0] += intensitysRGB[0];
                    intensitys[1] += intensitysRGB[1];
                    intensitys[2] += intensitysRGB[2];
                }
                else
                {
                    Point3D vectorL;
                    float tMax;

                    if (light.LType == LightType.Point)
                    {
                        vectorL = MyMath.Subtract(light.Position, point);
                        tMax = 1;
                    }
                    else // LightType.Directional
                    {
                        vectorL = light.Position;
                        tMax = float.MaxValue;
                    }

                    // Shadow check
                    float closestT;
                    Primitive blocker = ClosestIntersection(point, vectorL, MyMath.Epsilon, tMax, out closestT);
                    if (blocker != null)
                        continue;

                    // Диффузное/рассеянное отражение
                    float nDotL = MyMath.DotProduct(normal, vectorL);
                    if (nDotL > 0)
                    {
                        float tmp1 = nDotL / (lengthN * MyMath.Length(vectorL));
                        intensitys[0] += intensitysRGB[0] * tmp1;
                        intensitys[1] += intensitysRGB[1] * tmp1;
                        intensitys[2] += intensitysRGB[2] * tmp1;
                    }

                    // Зеркальное отражение
                    if (specular != -1)
                    {
                        Point3D vectorR = MyMath.ReflectRay(vectorL, normal);
                        float rDotV = MyMath.DotProduct(vectorR, view);
                        if (rDotV > 0)
                        {
                            float tmp2 = (float)Math.Pow(rDotV / (MyMath.Length(vectorR) * lengthV), specular);
                            intensitys[0] += intensitysRGB[0] * tmp2;
                            intensitys[1] += intensitysRGB[1] * tmp2;
                            intensitys[2] += intensitysRGB[2] * tmp2;
                        }
                    }
                }
            }

            return intensitys;
        }

        // Find the closest intersection between a ray and the primitives in the scene
        private Primitive ClosestIntersection(Point3D origin, Point3D direction, float minT, float maxT, out float closestT)
        {
            closestT = float.MaxValue;
            Primitive closestP = null;

            foreach (Primitive p in primitives)
            {
                float t = p.ClosestIntersection(origin, direction, minT, maxT);

                if (t < closestT)
                {
                    closestT = t;
                    closestP = p;
                }
            }

            return closestP;
        }

        // Traces a ray against the set of primitives in the scene
        private Point3D TraceRay(Point3D origin, Point3D direction, float minT, float maxT, int depth)
        {
            float closestT;
            Primitive closestP = ClosestIntersection(origin, direction, minT, maxT, out closestT);

            if (closestP == null)
                return backgroundColor;

            //return closestP.Color;

            // Точка пересечения с примитивом
            Point3D point = MyMath.Add(origin, MyMath.MultiplySV(closestT, direction));

            // Нормаль в точке пересечения
            Point3D normal = closestP.GetNormal(point);

            // Вектор, указывающий от объекта в камеру
            Point3D view = MyMath.MultiplySV(-1, direction);

            // Интенсивность света
            float[] lighting = ComputeLighting(point, normal, view, closestP.Specular);
            Point3D localColor = MyMath.MultiplySV(lighting, closestP.Color);

            // Здесь и ниже - отражение луча
            if (closestP.Reflective <= 0 || depth <= 0)
                return localColor;

            Point3D reflectedRay = MyMath.ReflectRay(view, normal);
            Point3D reflectedColor = TraceRay(point, reflectedRay, MyMath.Epsilon, float.MaxValue, depth - 1);

            return MyMath.Add(MyMath.MultiplySV(1 - closestP.Reflective, localColor),
                       MyMath.MultiplySV(closestP.Reflective, reflectedColor));
        }

        /*public void Start(int cameraIndex = 0)
        {
            System.Diagnostics.Stopwatch sw;

            //Pass the filepath and filename to the StreamWriter Constructor
            StreamWriter file = new StreamWriter("C:\\Test.txt");

            for (int i = 100; i < 1600; i += 100)
            {
                height = i;
                width = i;
                long time = 0;
                for (int j = 0; j < 10; j++)
                {
                    sw = new System.Diagnostics.Stopwatch();
                    int xStart = (int)(-width / 2f + 0.5f);
                    int xEnd = (int)(width / 2f + 0.5f);

                    float yStart = -height / 2f;
                    float yEnd = height / 2f;

                    Point3D cameraPosition = cameras[cameraIndex].Position;
                    float[,] cameraRotation = cameras[cameraIndex].Rotation;

                    sw.Start();
                    Parallel.For(xStart, xEnd, (x) =>
                    {
                        for (float y = yStart; y < yEnd; y++)
                        {
                            Point3D direction = CanvasToViewport(x, y);
                            direction = MyMath.MultiplyMV(cameraRotation, direction);
                            Point3D color = TraceRay(cameraPosition, direction, 1, float.MaxValue, RecursionDepth);
                        }
                    });
                    sw.Stop();
                    time += sw.ElapsedTicks;
                }

                //Write a line of text
                file.WriteLine(time / 10);
            }

            //Close the file
            file.Close();
        }*/

        public void Start(int cameraIndex = 0)
        {
            if (cameras.Count == 0)
                throw new Exception("There must be at least one camera in the scene.");

            canvasData = canvas.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);

            int xStart = (int)(-width / 2f + 0.5f);
            int xEnd = (int)(width / 2f + 0.5f);

            float yStart = -height / 2f;
            float yEnd = height / 2f;

            Point3D cameraPosition = cameras[cameraIndex].Position;
            float[,] cameraRotation = cameras[cameraIndex].Rotation;

            Parallel.For(xStart, xEnd, (x) =>
            {
                for (float y = yStart; y < yEnd; y++)
                {
                    Point3D direction = CanvasToViewport(x, y);
                    direction = MyMath.MultiplyMV(cameraRotation, direction);
                    Point3D color = TraceRay(cameraPosition, direction, 1, float.MaxValue, RecursionDepth);
                    PutPixel(x, y, color.Format24bppRgb);
                }
            });

            canvas.UnlockBits(canvasData);
        }

        public int FindClosest(int xScreen, int yScreen, ref Point3D normal, int cameraIndex = 0)
        {
            if (cameras.Count == 0)
                throw new Exception("There must be at least one camera in the scene.");

            xScreen = (int)(xScreen - width / 2f + 0.5);
            yScreen = (int)(height / 2f - yScreen + 0.5);

            Point3D direction = CanvasToViewport(xScreen, yScreen);
            direction = MyMath.MultiplyMV(cameras[cameraIndex].Rotation, direction);

            float closestT;
            Primitive closestP = ClosestIntersection(cameras[cameraIndex].Position, direction, 1, float.MaxValue, out closestT);

            if (closestP == null)
                return -1;
            else
            {
                normal = closestP.GetNormal(MyMath.Add(cameras[cameraIndex].Position, MyMath.MultiplySV(closestT, direction)));
                return primitives.IndexOf(closestP);
            }
        }
    }
}
