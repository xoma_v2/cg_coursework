﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework1
{
    public class TetrahedralPyramid : Primitive
    {
        /*public Point3D O
        {
            get { return o; }
            set
            {
                this.o = value;
                
                this.AO = MyMath.Subtract(o, a);
                this.CO = MyMath.Subtract(o, c);
                
                this.AOD = MyMath.Normalize(MyMath.CrossProduct(AD, AO));
                this.BOA = MyMath.Normalize(MyMath.CrossProduct(MyMath.MultiplySV(-1, AB), MyMath.Subtract(o, b)));
                this.COB = MyMath.Normalize(MyMath.CrossProduct(CB, CO));
                this.DOC = MyMath.Normalize(MyMath.CrossProduct(MyMath.MultiplySV(-1, CD), MyMath.Subtract(o, d)));
            }
        }
        public Point3D A
        {
            get { return a; }
            set
            {
                this.a = value;

                this.AD = MyMath.Subtract(d, a);
                this.AB = MyMath.Subtract(b, a);
                this.AO = MyMath.Subtract(o, a);

                this.ABCD = MyMath.Normalize(MyMath.CrossProduct(AB, AD));
                this.AOD = MyMath.Normalize(MyMath.CrossProduct(AD, AO));
                this.BOA = MyMath.Normalize(MyMath.CrossProduct(MyMath.MultiplySV(-1, AB), MyMath.Subtract(o, b)));
            }
        }
        public Point3D B
        {
            get { return b; }
            set
            {
                this.b = value;
                
                this.AB = MyMath.Subtract(b, a);
                this.CB = MyMath.Subtract(b, c);

                this.ABCD = MyMath.Normalize(MyMath.CrossProduct(AB, AD));
                this.BOA = MyMath.Normalize(MyMath.CrossProduct(MyMath.MultiplySV(-1, AB), MyMath.Subtract(o, b)));
                this.COB = MyMath.Normalize(MyMath.CrossProduct(CB, CO));
            }
        }
        public Point3D C
        {
            get { return c; }
            set
            {
                this.c = value;
                
                this.CD = MyMath.Subtract(d, c);
                this.CB = MyMath.Subtract(b, c);
                this.CO = MyMath.Subtract(o, c);
                
                this.COB = MyMath.Normalize(MyMath.CrossProduct(CB, CO));
                this.DOC = MyMath.Normalize(MyMath.CrossProduct(MyMath.MultiplySV(-1, CD), MyMath.Subtract(o, d)));
            }
        }
        public Point3D D
        {
            get { return d; }
            set
            {
                this.d = value;

                this.AD = MyMath.Subtract(d, a);
                this.CD = MyMath.Subtract(d, c);

                this.ABCD = MyMath.Normalize(MyMath.CrossProduct(AB, AD));
                this.AOD = MyMath.Normalize(MyMath.CrossProduct(AD, AO));
                this.DOC = MyMath.Normalize(MyMath.CrossProduct(MyMath.MultiplySV(-1, CD), MyMath.Subtract(o, d)));
            }
        }*/

        // Направляющие вектора
        private Point3D AD;
        private Point3D AB;
        private Point3D AO;
        private Point3D CD;
        private Point3D CB;
        private Point3D CO;

        // Нормали
        private Point3D ABCD;
        private Point3D AOD;
        private Point3D BOA;
        private Point3D COB;
        private Point3D DOC;

        // Вершины пирамиды
        public readonly Point3D o;
        public readonly Point3D a;
        public readonly Point3D b;
        public readonly Point3D c;
        public readonly Point3D d;

        // A, B, C, D - вершины основания
        // O - общая вершина пирамиды
        public TetrahedralPyramid(Point3D a, Point3D b, Point3D c, Point3D d, Point3D o, Point3D color, float specular, float reflective) : base(color, specular, reflective)
        {
            // TODO: Проверка на правильную ориентацию в пространстве, для корректного вычисления нормалей
            // TODO: Все точки основания должны быть на одной плоскости

            this.o = o;
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;

            this.AD = MyMath.Subtract(d, a);
            this.AB = MyMath.Subtract(b, a);
            this.AO = MyMath.Subtract(o, a);
            this.CD = MyMath.Subtract(d, c);
            this.CB = MyMath.Subtract(b, c);
            this.CO = MyMath.Subtract(o, c);

            this.ABCD = MyMath.Normalize(MyMath.CrossProduct(AB, AD));
            this.AOD = MyMath.Normalize(MyMath.CrossProduct(AD, AO));
            this.BOA = MyMath.Normalize(MyMath.CrossProduct(MyMath.MultiplySV(-1, AB), MyMath.Subtract(o, b)));
            this.COB = MyMath.Normalize(MyMath.CrossProduct(CB, CO));
            this.DOC = MyMath.Normalize(MyMath.CrossProduct(MyMath.MultiplySV(-1, CD), MyMath.Subtract(o, d)));
        }

        private static float FindTTriangle(Point3D P0, Point3D P01, Point3D P02, Point3D La, Point3D minusLab)
        {
            Point3D k1 = MyMath.Subtract(La, P0);
            float k2 = MyMath.DotProduct(minusLab, MyMath.CrossProduct(P01, P02));

            float u = MyMath.DotProduct(MyMath.CrossProduct(P02, minusLab), k1) / k2;
            if (u < 0 || u > 1)
                return float.MaxValue;

            float v = MyMath.DotProduct(MyMath.CrossProduct(minusLab, P01), k1) / k2;
            if (v < 0 || v > 1)
                return float.MaxValue;

            if ((u + v) > 1)
                return float.MaxValue;

            float t = MyMath.DotProduct(MyMath.CrossProduct(P01, P02), k1) / k2;
            return t;
        }

        public sealed override float ClosestIntersection(Point3D origin, Point3D direction, float minT, float maxT)
        {
            float t = float.MaxValue;
            float tmp;

            Point3D minusDirection = MyMath.MultiplySV(-1, direction);

            tmp = FindTTriangle(a, AD, AO, origin, minusDirection);
            if (minT < tmp && tmp < maxT && tmp < t)
                t = tmp;

            tmp = FindTTriangle(a, AB, AO, origin, minusDirection);
            if (minT < tmp && tmp < maxT && tmp < t)
                t = tmp;

            tmp = FindTTriangle(c, CB, CO, origin, minusDirection);
            if (minT < tmp && tmp < maxT && tmp < t)
                t = tmp;

            tmp = FindTTriangle(c, CD, CO, origin, minusDirection);
            if (minT < tmp && tmp < maxT && tmp < t)
                t = tmp;

            tmp = FindTTriangle(a, AD, AB, origin, minusDirection);
            if (minT < tmp && tmp < maxT && tmp < t)
                t = tmp;

            tmp = FindTTriangle(c, CD, CB, origin, minusDirection);
            if (minT < tmp && tmp < maxT && tmp < t)
                t = tmp;

            return t;
        }

        private static bool PointOnPlane(Point3D a, Point3D b, Point3D c, Point3D d, Point3D point)
        {
            Point3D ab = MyMath.Subtract(b, a);
            Point3D ad = MyMath.Subtract(d, a);
            Point3D ap = MyMath.Subtract(point, a);

            Point3D cd = MyMath.Subtract(d, c);
            Point3D cb = MyMath.Subtract(b, c);
            Point3D cp = MyMath.Subtract(point, c);

            float S = MyMath.Length(MyMath.CrossProduct(ad, ab));

            float S1 = 0.5f * MyMath.Length(MyMath.CrossProduct(ad, ap));
            float S2 = 0.5f * MyMath.Length(MyMath.CrossProduct(ap, ab));
            float S3 = 0.5f * MyMath.Length(MyMath.CrossProduct(cb, cp));
            float S4 = 0.5f * MyMath.Length(MyMath.CrossProduct(cp, cd));

            if (Math.Abs(S - S1 - S2 - S3 - S4) < MyMath.Epsilon)
                return true;
            else
                return false;
        }

        private static bool PointOnPlane(Point3D a, Point3D b, Point3D c, Point3D point)
        {
            Point3D ac = MyMath.Subtract(c, a);
            Point3D ab = MyMath.Subtract(b, a);
            Point3D ap = MyMath.Subtract(point, a);

            Point3D bc = MyMath.Subtract(c, b);
            Point3D bp = MyMath.Subtract(point, b);

            float S = 0.5f * MyMath.Length(MyMath.CrossProduct(ac, ab));

            float S1 = 0.5f * MyMath.Length(MyMath.CrossProduct(ac, ap));
            float S2 = 0.5f * MyMath.Length(MyMath.CrossProduct(ap, ab));
            float S3 = 0.5f * MyMath.Length(MyMath.CrossProduct(bp, bc));

            if (Math.Abs(S - S1 - S2 - S3) < MyMath.Epsilon)
                return true;
            else
                return false;
        }

        public sealed override Point3D GetNormal(Point3D point)
        {
            if (PointOnPlane(a, b, c, d, point))
                return ABCD;
            else if (PointOnPlane(a, o, d, point))
                return AOD;
            else if (PointOnPlane(b, o, a, point))
                return BOA;
            else if (PointOnPlane(c, o, b, point))
                return COB;
            else //if (PointOnPlane(d, o, c, point))
                return DOC;
        }

        public sealed override Point3D GetAveragePoint()
        {
            return MyMath.FindAverage(o, a, b, c, d);
        }
    }
}
