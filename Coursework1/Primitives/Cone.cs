﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Coursework1
{
    public class Cone : Primitive
    {
        // Центр "нижнего" основания
        public readonly Point3D Center;

        // Высота цилиндра
        public readonly float Height;

        // Радиус оснований
        public readonly float Radius;

        // squared cosine and sine of the angle
        // at the cone top
        private float cosAlphaSq;
        private float sinAlphaSq;

        // Цилиндр задаётся центром основания, радиусом окружности в основании, и высотой цилиндра
        public Cone(Point3D center, float radius, float height, Point3D color, float specular, float reflective) : base(color, specular, reflective)
        {
            this.Radius = radius;
            if (height > 0)
            {
                this.Center = center;
                this.Height = height;
            }
            else
            {
                this.Center = new Point3D(center.X, center.Y + height, center.Z);
                this.Height = height * -1;
            }

            //calculateAngle();

            Point3D p1 = new Point3D(Center.X + radius, Center.Y, Center.Z);
            Point3D p2 = new Point3D(Center.X, Center.Y + height, Center.Z);

            float d = MyMath.Length(MyMath.Subtract(p2, p1));

            cosAlphaSq = height / d;
            sinAlphaSq = radius / d;

            MessageBox.Show(cosAlphaSq.ToString() + " " + sinAlphaSq.ToString());
        }

        // calculate the angle at the cone vertex
        private void calculateAngle()
        {
            Point3D p1 = new Point3D(Center.X + Radius, Center.Y, Center.Z);
            Point3D p2 = new Point3D(Center.X, Center.Y + Height, Center.Z);

            float d = MyMath.Length(MyMath.Subtract(p2, p1));

            cosAlphaSq = Height / d;
            sinAlphaSq = Radius / d;
        }

        public sealed override float ClosestIntersection(Point3D origin, Point3D direction, float minT, float maxT)
        {
            /*float x = (origin.X - Center.X);
            float y = (origin.Y - Center.Y);
            float z = (origin.Z - Center.Z);

            float yMin = Center.Y;
            float yMax = Center.Y + Height;

            //float t3 = (yMin - origin.Y) / direction.Y;
            //float t4 = (yMax - origin.Y) / direction.Y;

            float a = direction.X * direction.X + direction.Z * direction.Z - direction.Y * direction.Y;
            float b = 2 * (direction.X * x + direction.Z * z - direction.Y * y);
            float c = x * x + z * z - y * y;

            float discriminant = b * b - 4 * a * c;
            if (discriminant < 0)
                return float.MaxValue;

            float t1 = (-b + (float)Math.Sqrt(discriminant)) / (2 * a);
            float t2 = (-b - (float)Math.Sqrt(discriminant)) / (2 * a);

            float y1 = origin.Y + t1 * direction.Y;
            float y2 = origin.Y + t2 * direction.Y;

            //if (t1 <= t2 && yMin < y1 && y1 < yMax && minT < t1 && t1 < maxT)
            //    return t1;
            //else if (t2 < t1 && yMin < y2 && y2 < yMax && minT < t2 && t2 < maxT)
            //    return t2;
            if (t1 <= t2 && minT < t1 && t1 < maxT)
                return t1;
            else if (t2 < t1 && minT < t2 && t2 < maxT)
                return t2;
            else
                return float.MaxValue;*/
            
            // translate ray origin so that the cone vertex would be at the origin
            Point3D p0 = new Point3D(origin.X - Center.X, origin.Y - Center.Y - Height, origin.Z - Center.Z);

            // coefficients for the intersection equation
            // got them mathematically intersecting the line equation with the cone equation
            float a = cosAlphaSq * direction.X * direction.X + cosAlphaSq * direction.Z * direction.Z - sinAlphaSq * direction.Y * direction.Y;
            float b = cosAlphaSq * direction.X * p0.X + cosAlphaSq * direction.Z * p0.Z - sinAlphaSq * direction.Y * p0.Y;
            float c = cosAlphaSq * p0.X * p0.X + cosAlphaSq * p0.Z * p0.Z - sinAlphaSq * p0.Y * p0.Y;

            float delta = b * b - a * c;

            // use epsilon because of computation errors between doubles

            // delta < 0 means no intersections
            if (delta < 0)
                return float.MaxValue;

            // nearest intersection
            float t = (-b - (float)Math.Sqrt(delta)) / a;

            // t < 0 means the intersection is behind the ray origin
            // which we don't want
            if (t < minT)
                return float.MaxValue;

            float y = p0.Y + t * direction.Y;

            // if the intersection with the cone is lower than
            // the base of the cone (y < -height) or higher than the vertex (y > 0)
            // it's not actually intersecting our cone
            if (y < -Height || y > 0)
                return float.MaxValue;

            return t;
        }

        public sealed override Point3D GetNormal(Point3D point)
        {
            Point3D c0 = new Point3D(Center.X, point.Y, Center.Z);
            Point3D c1 = new Point3D(Center.X, Center.Y + Height, Center.Z);

            Point3D v = MyMath.Subtract(point, Center);
            v.Y = 0;
            MyMath.Normalize(v);

            Point3D n = new Point3D(v.X * Height / Radius,
                                    Radius / Height,
                                    v.Z * Height / Radius);
            return n;
        }

        public sealed override Point3D GetAveragePoint()
        {
            return new Point3D(Center.X, Center.Y + Height / 2f, Center.Z);
        }
    }
}
