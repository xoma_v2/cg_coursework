﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework1
{
    public class TriangularPrism : Primitive
    {
        /*public Point3D Center
        {
            get { return A; }
            set
            {
                this.O = value;
                this.OX = MyMath.Subtract(X, O);
                this.OY = MyMath.Subtract(Y, O);
                this.OZ = MyMath.Subtract(Z, O);

                this.A = O;

                this.B1 = MyMath.Add(B, OZ);
                this.C1 = MyMath.Add(C, OZ);

                this.A1B1C1 = MyMath.Normalize(MyMath.CrossProduct(OX, OY));
                this.ABC = MyMath.Normalize(MyMath.CrossProduct(OY, OX));
                this.AA1C = MyMath.Normalize(MyMath.CrossProduct(OX, OZ));
                this.BB1A = MyMath.Normalize(MyMath.CrossProduct(OZ, OY));
                this.CC1B = MyMath.Normalize(MyMath.CrossProduct(XY, OZ));
            }
        }
        public Point3D X
        {
            get { return C; }
            set
            {
                this.OX = MyMath.Subtract(value, O);
                this.XY = MyMath.Subtract(Y, value);
                
                this.C = value;
                
                this.C1 = MyMath.Add(C, OZ);

                this.A1B1C1 = MyMath.Normalize(MyMath.CrossProduct(OX, OY));
                this.ABC = MyMath.Normalize(MyMath.CrossProduct(OY, OX));
                this.AA1C = MyMath.Normalize(MyMath.CrossProduct(OX, OZ));
                this.CC1B = MyMath.Normalize(MyMath.CrossProduct(XY, OZ));
            }
        }
        public Point3D Y
        {
            get { return B; }
            set
            {
                this.OY = MyMath.Subtract(value, O);
                this.XY = MyMath.Subtract(value, X);
                
                this.B = value;
                
                this.B1 = MyMath.Add(B, OZ);

                this.A1B1C1 = MyMath.Normalize(MyMath.CrossProduct(OX, OY));
                this.ABC = MyMath.Normalize(MyMath.CrossProduct(OY, OX));
                this.BB1A = MyMath.Normalize(MyMath.CrossProduct(OZ, OY));
                this.CC1B = MyMath.Normalize(MyMath.CrossProduct(XY, OZ));
            }
        }
        public Point3D Z
        {
            get { return A1; }
            set
            {
                this.OZ = MyMath.Subtract(value, O);
                
                this.A1 = value;
                this.B1 = MyMath.Add(B, OZ);
                this.C1 = MyMath.Add(C, OZ);
                
                this.AA1C = MyMath.Normalize(MyMath.CrossProduct(OX, OZ));
                this.BB1A = MyMath.Normalize(MyMath.CrossProduct(OZ, OY));
                this.CC1B = MyMath.Normalize(MyMath.CrossProduct(XY, OZ));
            }
        }*/

        // Вершина и направляющие вектора
        public readonly Point3D O;
        public readonly Point3D OX;
        public readonly Point3D OY;
        public readonly Point3D OZ;
        private Point3D XY;

        // Нормали
        private Point3D ABC;
        private Point3D A1B1C1;

        private Point3D AA1C;
        private Point3D BB1A;
        private Point3D CC1B;

        // Вершины на "нижней" грани 
        public readonly Point3D A;
        public readonly Point3D B;
        public readonly Point3D C;

        // Вершины на "верхней" грани
        public readonly Point3D A1;
        public readonly Point3D B1;
        public readonly Point3D C1;

        // O, X, Y - вершины нижнего основания
        // Z - вершина верхнего основания, связанная ребром с O
        public TriangularPrism(Point3D O, Point3D X, Point3D Y, Point3D Z, Point3D color, float specular, float reflective) : base(color, specular, reflective)
        {
            // TODO: Проверка на правильную ориентацию в пространстве, для корректного вычисления нормалей

            this.O = O;
            this.OX = MyMath.Subtract(X, O);
            this.OY = MyMath.Subtract(Y, O);
            this.OZ = MyMath.Subtract(Z, O);
            this.XY = MyMath.Subtract(Y, X);

            this.A = O;
            this.B = Y;
            this.C = X;

            this.A1 = Z;
            this.B1 = MyMath.Add(B, OZ);
            this.C1 = MyMath.Add(C, OZ);

            this.A1B1C1 = MyMath.Normalize(MyMath.CrossProduct(OX, OY));
            this.ABC = MyMath.Normalize(MyMath.CrossProduct(OY, OX));
            this.AA1C = MyMath.Normalize(MyMath.CrossProduct(OX, OZ));
            this.BB1A = MyMath.Normalize(MyMath.CrossProduct(OZ, OY));
            this.CC1B = MyMath.Normalize(MyMath.CrossProduct(XY, OZ));
        }

        private static float FindTParallelogram(Point3D P0, Point3D P01, Point3D P02, Point3D La, Point3D minusLab)
        {
            Point3D k1 = MyMath.Subtract(La, P0);
            float k2 = MyMath.DotProduct(minusLab, MyMath.CrossProduct(P01, P02));

            float u = MyMath.DotProduct(MyMath.CrossProduct(P02, minusLab), k1) / k2;
            if (u < 0 || u > 1)
                return float.MaxValue;

            float v = MyMath.DotProduct(MyMath.CrossProduct(minusLab, P01), k1) / k2;
            if (v < 0 || v > 1)
                return float.MaxValue;

            float t = MyMath.DotProduct(MyMath.CrossProduct(P01, P02), k1) / k2;
            return t;
        }

        private static float FindTTriangle(Point3D P0, Point3D P01, Point3D P02, Point3D La, Point3D minusLab)
        {
            Point3D k1 = MyMath.Subtract(La, P0);
            float k2 = MyMath.DotProduct(minusLab, MyMath.CrossProduct(P01, P02));

            float u = MyMath.DotProduct(MyMath.CrossProduct(P02, minusLab), k1) / k2;
            if (u < 0 || u > 1)
                return float.MaxValue;

            float v = MyMath.DotProduct(MyMath.CrossProduct(minusLab, P01), k1) / k2;
            if (v < 0 || v > 1)
                return float.MaxValue;

            if ((u + v) > 1)
                return float.MaxValue;

            float t = MyMath.DotProduct(MyMath.CrossProduct(P01, P02), k1) / k2;
            return t;
        }

        public sealed override float ClosestIntersection(Point3D origin, Point3D direction, float minT, float maxT)
        {
            float t = float.MaxValue;
            float tmp;

            Point3D minusDirection = MyMath.MultiplySV(-1, direction);

            tmp = FindTTriangle(O, OY, OX, origin, minusDirection);
            if (minT < tmp && tmp < maxT && tmp < t)
                t = tmp;

            tmp = FindTTriangle(A1, OY, OX, origin, minusDirection);
            if (minT < tmp && tmp < maxT && tmp < t)
                t = tmp;

            tmp = FindTParallelogram(O, OX, OZ, origin, minusDirection);
            if (minT < tmp && tmp < maxT && tmp < t)
                t = tmp;

            tmp = FindTParallelogram(O, OY, OZ, origin, minusDirection);
            if (minT < tmp && tmp < maxT && tmp < t)
                t = tmp;

            tmp = FindTParallelogram(C, XY, OZ, origin, minusDirection);
            if (minT < tmp && tmp < maxT && tmp < t)
                t = tmp;

            return t;
        }

        private static bool PointOnPlane(Point3D a, Point3D b, Point3D c, Point3D d, Point3D point)
        {
            Point3D ab = MyMath.Subtract(b, a);
            Point3D ad = MyMath.Subtract(d, a);
            Point3D ap = MyMath.Subtract(point, a);
            
            Point3D cd = MyMath.Subtract(d, c);
            Point3D cb = MyMath.Subtract(b, c);
            Point3D cp = MyMath.Subtract(point, c);

            float S = MyMath.Length(MyMath.CrossProduct(ad, ab));

            float S1 = 0.5f * MyMath.Length(MyMath.CrossProduct(ad, ap));
            float S2 = 0.5f * MyMath.Length(MyMath.CrossProduct(ap, ab));
            float S3 = 0.5f * MyMath.Length(MyMath.CrossProduct(cb, cp));
            float S4 = 0.5f * MyMath.Length(MyMath.CrossProduct(cp, cd));

            if (Math.Abs(S - S1 - S2 - S3 - S4) < MyMath.Epsilon)
                return true;
            else
                return false;
        }

        private static bool PointOnPlane(Point3D a, Point3D b, Point3D c, Point3D point)
        {
            Point3D ac = MyMath.Subtract(c, a);
            Point3D ab = MyMath.Subtract(b, a);
            Point3D ap = MyMath.Subtract(point, a);
            
            Point3D bc = MyMath.Subtract(c, b);
            Point3D bp = MyMath.Subtract(point, b);

            float S = 0.5f * MyMath.Length(MyMath.CrossProduct(ac, ab));

            float S1 = 0.5f * MyMath.Length(MyMath.CrossProduct(ac, ap));
            float S2 = 0.5f * MyMath.Length(MyMath.CrossProduct(ap, ab));
            float S3 = 0.5f * MyMath.Length(MyMath.CrossProduct(bp, bc));

            if (Math.Abs(S - S1 - S2 - S3) < MyMath.Epsilon)
                return true;
            else
                return false;
        }

        public sealed override Point3D GetNormal(Point3D point)
        {
            if (PointOnPlane(A, A1, C1, C, point))
                return AA1C;
            else if (PointOnPlane(B, B1, A1, A, point))
                return BB1A;
            else if (PointOnPlane(C, C1, B1, B, point))
                return CC1B;
            else if (PointOnPlane(A, B, C, point))
                return ABC;
            else //if (PointOnPlane(A1, B1, C1, point))
                return A1B1C1;
        }

        public sealed override Point3D GetAveragePoint()
        {
            return MyMath.FindAverage(A, B, C, A1, B1, C1);
        }
    }
}
