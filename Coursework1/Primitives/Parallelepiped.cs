﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework1
{
    public class Parallelepiped : Primitive
    {
        /*public Point3D Center
        {
            get { return A; }
            set
            {
                this.O = value;
                this.OX = MyMath.Subtract(X, O);
                this.OY = MyMath.Subtract(Y, O);
                this.OZ = MyMath.Subtract(Z, O);

                this.A = O;
                this.C = MyMath.Add(B, OX);
                
                this.B1 = MyMath.Add(B, OZ);
                this.C1 = MyMath.Add(C, OZ);
                this.D1 = MyMath.Add(D, OZ);

                this.anotherO = MyMath.Add(MyMath.Add(Y, OX), OZ);
                this.XO = MyMath.Subtract(O, X);
                this.YO = MyMath.Subtract(O, Y);
                this.ZO = MyMath.Subtract(O, Z);

                this.OXY = MyMath.Normalize(MyMath.CrossProduct(OY, OX));
                this.OXZ = MyMath.Normalize(MyMath.CrossProduct(OX, OZ));
                this.OYZ = MyMath.Normalize(MyMath.CrossProduct(OZ, OY));

                this.minusOXY = MyMath.MultiplySV(-1, OXY);
                this.minusOXZ = MyMath.MultiplySV(-1, OXZ);
                this.minusOYZ = MyMath.MultiplySV(-1, OYZ);
            }
        }
        public Point3D X
        {
            get { return D; }
            set
            {
                this.OX = MyMath.Subtract(value, O);
                
                this.C = MyMath.Add(B, OX);
                this.D = value;
                
                this.C1 = MyMath.Add(C, OZ);
                this.D1 = MyMath.Add(D, OZ);

                this.anotherO = MyMath.Add(MyMath.Add(Y, OX), OZ);
                this.XO = MyMath.Subtract(O, value);

                this.OXY = MyMath.Normalize(MyMath.CrossProduct(OY, OX));
                this.OXZ = MyMath.Normalize(MyMath.CrossProduct(OX, OZ));

                this.minusOXY = MyMath.MultiplySV(-1, OXY);
                this.minusOXZ = MyMath.MultiplySV(-1, OXZ);
            }
        }
        public Point3D Y
        {
            get { return B; }
            set
            {
                this.OY = MyMath.Subtract(value, O);
                
                this.B = value;
                this.C = MyMath.Add(B, OX);
                
                this.B1 = MyMath.Add(B, OZ);
                this.C1 = MyMath.Add(C, OZ);

                this.anotherO = MyMath.Add(MyMath.Add(value, OX), OZ);
                this.YO = MyMath.Subtract(O, value);

                this.OXY = MyMath.Normalize(MyMath.CrossProduct(OY, OX));
                this.OYZ = MyMath.Normalize(MyMath.CrossProduct(OZ, OY));

                this.minusOXY = MyMath.MultiplySV(-1, OXY);
                this.minusOYZ = MyMath.MultiplySV(-1, OYZ);
            }
        }
        public Point3D Z
        {
            get { return A1; }
            set
            {
                this.OZ = MyMath.Subtract(value, O);
                
                this.A1 = value;
                this.B1 = MyMath.Add(B, OZ);
                this.C1 = MyMath.Add(C, OZ);
                this.D1 = MyMath.Add(D, OZ);

                this.anotherO = MyMath.Add(MyMath.Add(Y, OX), OZ);
                this.ZO = MyMath.Subtract(O, value);
                
                this.OXZ = MyMath.Normalize(MyMath.CrossProduct(OX, OZ));
                this.OYZ = MyMath.Normalize(MyMath.CrossProduct(OZ, OY));
                
                this.minusOXZ = MyMath.MultiplySV(-1, OXZ);
                this.minusOYZ = MyMath.MultiplySV(-1, OYZ);
            }
        }*/

        // Вершина и направляющие вектора
        public readonly Point3D O;
        public readonly Point3D OX;
        public readonly Point3D OY;
        public readonly Point3D OZ;

        // Противположная вершина и противоположные направляющие вектора
        private Point3D anotherO;
        private Point3D XO;
        private Point3D YO;
        private Point3D ZO;

        // Нормали
        private Point3D OXY;
        private Point3D OXZ;
        private Point3D OYZ;

        // Противоположные нормали
        private Point3D minusOXY;
        private Point3D minusOXZ;
        private Point3D minusOYZ;

        // Вершины на "нижней" грани
        public readonly Point3D A;
        public readonly Point3D B;
        public readonly Point3D C;
        public readonly Point3D D;

        // Вершины на "верхней" грани
        public readonly Point3D A1;
        public readonly Point3D B1;
        public readonly Point3D C1;
        public readonly Point3D D1;

        // O, X, Y - вершины нижнего основания
        // Z - вершина верхнего основания, связанная ребром с O
        public Parallelepiped(Point3D o, Point3D x, Point3D y, Point3D z, Point3D color, float specular, float reflective) : base(color, specular, reflective)
        {
            // TODO: Проверка на правильную ориентацию в пространстве, для корректного вычисления нормалей
            
            this.O = o;
            this.OX = MyMath.Subtract(x, o);
            this.OY = MyMath.Subtract(y, o);
            this.OZ = MyMath.Subtract(z, o);

            this.A = o;
            this.B = y;
            this.C = MyMath.Add(B, OX);
            this.D = x;

            this.A1 = z;
            this.B1 = MyMath.Add(B, OZ);
            this.C1 = MyMath.Add(C, OZ);
            this.D1 = MyMath.Add(D, OZ);

            this.anotherO = MyMath.Add(MyMath.Add(y, OX), OZ);
            this.XO = MyMath.Subtract(o, x);
            this.YO = MyMath.Subtract(o, y);
            this.ZO = MyMath.Subtract(o, z);

            this.OXY = MyMath.Normalize(MyMath.CrossProduct(OY, OX));
            this.OXZ = MyMath.Normalize(MyMath.CrossProduct(OX, OZ));
            this.OYZ = MyMath.Normalize(MyMath.CrossProduct(OZ, OY));

            this.minusOXY = MyMath.MultiplySV(-1, OXY);
            this.minusOXZ = MyMath.MultiplySV(-1, OXZ);
            this.minusOYZ = MyMath.MultiplySV(-1, OYZ);
        }

        private static float FindT(Point3D P0, Point3D P01, Point3D P02, Point3D La, Point3D minusLab)
        {
            Point3D k1 = MyMath.Subtract(La, P0);
            float k2 = MyMath.DotProduct(minusLab, MyMath.CrossProduct(P01, P02));

            float u = MyMath.DotProduct(MyMath.CrossProduct(P02, minusLab), k1) / k2;
            if (u < 0 || u > 1)
                return float.MaxValue;

            float v = MyMath.DotProduct(MyMath.CrossProduct(minusLab, P01), k1) / k2;
            if (v < 0 || v > 1)
                return float.MaxValue;

            float t = MyMath.DotProduct(MyMath.CrossProduct(P01, P02), k1) / k2;
            return t;
        }

        public sealed override float ClosestIntersection(Point3D origin, Point3D direction, float minT, float maxT)
        {
            float t = float.MaxValue;
            float tmp;

            Point3D minusDirection = MyMath.MultiplySV(-1, direction);

            tmp = FindT(O, OY, OX, origin, minusDirection);
            if (minT < tmp && tmp < maxT && tmp < t)
                t = tmp;

            tmp = FindT(anotherO, XO, YO, origin, minusDirection);
            if (minT < tmp && tmp < maxT && tmp < t)
                t = tmp;

            tmp = FindT(O, OX, OZ, origin, minusDirection);
            if (minT < tmp && tmp < maxT && tmp < t)
                t = tmp;

            tmp = FindT(anotherO, ZO, XO, origin, minusDirection);
            if (minT < tmp && tmp < maxT && tmp < t)
                t = tmp;

            tmp = FindT(O, OZ, OY, origin, minusDirection);
            if (minT < tmp && tmp < maxT && tmp < t)
                t = tmp;

            tmp = FindT(anotherO, YO, ZO, origin, minusDirection);
            if (minT < tmp && tmp < maxT && tmp < t)
                t = tmp;

            return t;
        }

        private static bool PointOnPlane(Point3D a, Point3D b, Point3D c, Point3D d, Point3D point)
        {
            Point3D ab = MyMath.Subtract(b, a);
            Point3D ad = MyMath.Subtract(d, a);
            Point3D ap = MyMath.Subtract(point, a);

            Point3D cd = MyMath.Subtract(d, c);
            Point3D cb = MyMath.Subtract(b, c);
            Point3D cp = MyMath.Subtract(point, c);

            float S = MyMath.Length(MyMath.CrossProduct(ad, ab));

            float S1 = 0.5f * MyMath.Length(MyMath.CrossProduct(ad, ap));
            float S2 = 0.5f * MyMath.Length(MyMath.CrossProduct(ap, ab));
            float S3 = 0.5f * MyMath.Length(MyMath.CrossProduct(cb, cp));
            float S4 = 0.5f * MyMath.Length(MyMath.CrossProduct(cp, cd));

            if (Math.Abs(S - S1 - S2 - S3 - S4) < MyMath.Epsilon)
                return true;
            else
                return false;
        }

        public sealed override Point3D GetNormal(Point3D point)//, float t, Point3D origin, Point3D direction)
        {
            if (PointOnPlane(A, B, C, D, point))
                return OXY;
            else if (PointOnPlane(A, A1, D1, D, point))
                return OXZ;
            else if (PointOnPlane(B, B1, A1, A, point))
                return OYZ;
            else if (PointOnPlane(A1, B1, C1, D1, point))
                return minusOXY;
            else if (PointOnPlane(C, C1, B1, B, point))
                return minusOXZ;
            else //if (PointOnPlane(D, D1, C1, C, point))
                return minusOYZ;
        }

        public sealed override Point3D GetAveragePoint()
        {
            return MyMath.FindAverage(A, B, C, D, A1, B1, C1, D1);
        }
    }
}
