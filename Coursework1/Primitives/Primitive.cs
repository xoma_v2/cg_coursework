﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework1
{
    public abstract class Primitive
    {
        public readonly Point3D Color;
        public readonly float Specular;
        public readonly float Reflective;

        public Primitive(Point3D color, float specular, float reflective)
        {
            this.Color = color;
            this.Specular = specular;
            this.Reflective = reflective;
        }

        public abstract float ClosestIntersection(Point3D origin, Point3D direction, float minT, float maxT);
        public abstract Point3D GetNormal(Point3D point);
        public abstract Point3D GetAveragePoint();
    }
}
