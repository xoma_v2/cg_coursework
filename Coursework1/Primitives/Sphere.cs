﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework1
{
    public class Sphere : Primitive
    {
        public readonly Point3D Center;
        public readonly float Radius;

        public Sphere(Point3D center, float radius, Point3D color, float specular, float reflective) : base(color, specular, reflective)
        {
            this.Center = center;
            this.Radius = radius;
        }

        // Computes the intersection of a ray and a sphere.
        // Returns the walus of t for the intersections.
        public sealed override float ClosestIntersection(Point3D origin, Point3D direction, float minT, float maxT)
        {
            Point3D OC = MyMath.Subtract(origin, Center);

            float a = MyMath.DotProduct(direction, direction);
            float b = 2 * MyMath.DotProduct(OC, direction);
            float c = MyMath.DotProduct(OC, OC) - Radius * Radius;

            float discriminant = b * b - 4 * a * c;
            if (discriminant < 0)
                return float.MaxValue;

            float t1 = (-b + (float)Math.Sqrt(discriminant)) / (2 * a);
            float t2 = (-b - (float)Math.Sqrt(discriminant)) / (2 * a);

            if (t1 <= t2 && minT < t1 && t1 < maxT)
                return t1;
            else if (t2 < t1 && minT < t2 && t2 < maxT)
                return t2;
            else
                return float.MaxValue;
        }

        public sealed override Point3D GetNormal(Point3D point)
        {
            Point3D normal = MyMath.Subtract(point, Center);
            return MyMath.Normalize(normal);
        }

        public sealed override Point3D GetAveragePoint()
        {
            return Center;
        }
    }
}