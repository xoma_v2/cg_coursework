﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Coursework1
{
    public enum CylinderType { OX = 0, OY, OZ };

    public class Cylinder : Primitive
    {
        // Центр "нижнего" основания
        public readonly Point3D Center;

        // Высота цилиндра
        public readonly float Height;

        // Радиус оснований
        public readonly float Radius;

        // Цилиндр задаётся центром основания, радиусом окружности в основании, и высотой цилиндра
        public Cylinder(Point3D center, float radius, float height, Point3D color, float specular, float reflective) : base(color, specular, reflective)
        {
            this.Radius = radius;
            if (height > 0)
            {
                this.Center = center;
                this.Height = height;
            }
            else
            {
                this.Center = new Point3D(center.X, center.Y + height, center.Z);
                this.Height = height * -1;
            }
        }

        // Calculate intersection with the base having center c
        // We do this by calculating the intersection with the plane
        // and then checking if the intersection is within the base circle
        private float BaseIntersection(Point3D origin, Point3D direction, Point3D c)
        {
            Point3D normal = GetNormal(c);
            Point3D p0 = new Point3D(origin.X - Center.X, origin.Y - Center.Y, origin.Z - Center.Z);
            float A = normal[0];
            float B = normal[1];
            float C = normal[2];
            float D = -(A * (c.X - Center.X) + B * (c.Y - Center.Y) + C * (c.Z - Center.Z));

            if (A * direction[0] + B * direction[1] + C * direction[2] == 0)
                return float.MaxValue;

            float dist = -(A * p0.X + B * p0.Y + C * p0.Z + D) / (A * direction[0] + B * direction[1] + C * direction[2]);

            if (dist < MyMath.Epsilon)
                return float.MaxValue;

            Point3D p = new Point3D(p0.X + dist * direction[0],
                                    p0.Y + dist * direction[1],
                                    p0.Z + dist * direction[2]);
            if (p.X * p.X + p.Z * p.Z - Radius * Radius > MyMath.Epsilon)
                return float.MaxValue;
            
            return dist;
        }

        public sealed override float ClosestIntersection(Point3D origin, Point3D direction, float minT, float maxT)
        {
            // translate the ray origin
            Point3D p = new Point3D(origin.X - Center.X, origin.Y - Center.Y, origin.Z - Center.Z);

            // coefficients for the intersection equation
            // got them mathematically intersecting the line equation with the cylinder equation
            float a = direction.X * direction.X + direction.Z * direction.Z;
            float b = direction.X * p.X + direction.Z * p.Z;
            float c = p.X * p.X + p.Z * p.Z - Radius * Radius;

            float delta = b * b - a * c;

            // use epsilon because of computation errors between doubles

            // delta < 0 means no intersections
            if (delta < 0)
                return float.MaxValue;

            // nearest intersection
            float t = (-b - (float)Math.Sqrt(delta)) / a;

            // t < minT means the intersection is behind the ray origin
            // which we don't want
            if (t <= minT)
                return float.MaxValue;

            float y = p.Y + t * direction.Y;

            // check if we intersect one of the bases
            if (y > Height || y < 0)
            {
                float t1 = BaseIntersection(origin, direction, Center);
                float t2 = BaseIntersection(origin, direction, new Point3D(Center.X, Center.Y + Height, Center.Z));

                if (t1 <= t2)
                    return t1;
                else
                    return t2;
            }

            if (minT < t && t < maxT)
                return t;
            else
                return float.MaxValue;
        }

        public sealed override Point3D GetNormal(Point3D point)
        {
            // Point is on one of the bases
            if ((Center.X - Radius) < point.X && point.X < (Center.X + Radius) && (Center.Z - Radius) < point.Z && point.Z < (Center.Z + Radius))
            {
                if (Math.Abs(point.Y - Center.Y - Height) < MyMath.Epsilon)
                    return new Point3D(0, 1, 0);

                if (Math.Abs(point.Y - Center.Y) < MyMath.Epsilon)
                    return new Point3D(0, -1, 0);
            }

            // Point is on lateral surface
            Point3D c0 = new Point3D(Center.X, point.Y, Center.Z);
            return MyMath.Normalize(MyMath.Subtract(point, c0));
        }

        public sealed override Point3D GetAveragePoint()
        {
            return new Point3D(Center.X, Center.Y + Height / 2f, Center.Z);
        }
    }
}
